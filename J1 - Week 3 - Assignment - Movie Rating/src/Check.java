//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 3 - Assignment - Decision Structures
//#################################################################################################
//Create a program that prompts the user to enter two values: a movie rating and his or her age.
//Using a decision structure, determine whether the user would be allowed to see the movie in a
//Theater based on the rating and age entered. Finally, display the result of this decision to
//the user. If you are not familiar with movie ratings, read about them here:
//http://www.mpaa.org/ratings
//#################################################################################################

public class Check { //begin class
	private int age; //stores age
	private String rating; //stores rating

	public void verification() { //begin method
		switch (this.rating) //switch
		{ //begin switch
		case "G": //G rated movie
			if (this.age > 0) { //begin if
				System.out.println("Approved"); //valid age
			}else{ //begin else
				System.out.println("Not born yet, Denied"); //non valid age
			} //end else
			break; //exit switch
			
		case "PG": //PG rated movie
			if (this.age > 0) { //begin if
				System.out.println("Approved"); //valid age
			}else{ //begin else
				System.out.println("Not born yet, Denied"); //non valid age
			} //end else
			break; //exit switch
			
		case "PG-13": //PG-13 rated movie
			if (this.age > 13) { //begin if
				System.out.println("Approved"); //valid age
			}else{ //begin else
				System.out.println("Denied"); //non valid age
			} //end else
			break; //exit switch
			
		case "R": //R rated movie
			if (this.age >= 17) { //begin if
				System.out.println("Approved"); //valid age
			}else{ //begin else
				System.out.println("Denied"); //non valid age
			} //end else
			break; //exit switch
			
		case "NC-17": //NC-17 rated movie
			if (this.age >= 18) { //begin if
				System.out.println("Approved"); //valid age
			}else{ //begin else
				System.out.println("Denied"); //non valid age
			} //end else
			break; //exit switch
		}  //end switch
	} //end method

	public int getAge() { //begin method
		return this.age; //returns age
	} //end method

	public void setAge(int age) { //begins method
		this.age = age; //sets age
	} //end method

	public String getRating() { //begin method
		return this.rating; //returns rating
	} //end method

	public void setRating(String rating) { //begin method
		this.rating = rating; //sets rating
	} //end method
} //end class