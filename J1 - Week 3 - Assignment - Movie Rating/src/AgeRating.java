//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 3 - Assignment - Decision Structures
//#################################################################################################
//Create a program that prompts the user to enter two values: a movie rating and his or her age.
//Using a decision structure, determine whether the user would be allowed to see the movie in a
//Theater based on the rating and age entered. Finally, display the result of this decision to
//the user. If you are not familiar with movie ratings, read about them here:
//http://www.mpaa.org/ratings
//#################################################################################################

import java.util.Scanner;

public class AgeRating { //begin class
	public static void main(String[] args) { //begin main
	
	Check userCheck = new Check(); //creating Check object
	Scanner i = new Scanner(System.in); //opening scanner object

	enterAge(userCheck, i); //method for entering users age
	enterRating(userCheck, i); //method for entering the rating of the movie
	
	System.out.println("");
	System.out.println("Age: " + userCheck.getAge() ); //displaying age entered
	System.out.println("Rating: " + userCheck.getRating() ); //displaying rating entered
	
	userCheck.verification(); //running the verification on the rating/age
	
	i.close(); //closing scanner object
	}
	
	//method for entering users age
	static void enterAge(Check user, Scanner i) //method passing through object and scanner
	{ //begin method
		boolean shouldExit = false; //exit variable
		while (!shouldExit) //while loop
		{ //begin loop
			System.out.println(" "); //for display
			System.out.print("Please type your age: "); //requesting age
			if (i.hasNextInt()) //verifying input is an integer
			{ //if true
				int temp = i.nextInt(); //saving into the temp variable
				if (temp > 0){ //making sure value is positive (I know its checked also in Check)
					user.setAge(temp); //if its an integer save
					i.nextLine(); //to remove the return
					shouldExit = true; //setting exit variable
				}else{ //begin else
					System.out.println("Invalid Input"); //prompt if invalid
					i.nextLine(); //to remove the return
				}
				
			}else{ //begin else
				System.out.println("Invalid Input"); //prompt if invalid
				i.nextLine(); //to remove the return
			} //end else
		} //end loop
	} //end method
	
	//method for entering the rating of the movie
	static void enterRating(Check user, Scanner i) //method passing through object and scanner
	{ //begin method
		boolean shouldExit = false; //setting exit variable
		while (!shouldExit) //while loop
		{ //begin loop
			System.out.println(" "); //for display
			System.out.println("Available Ratings: G, PG, PG-13, R, NC-17"); //showing valid ratings
			System.out.print("Please type movie rating: "); //prompting for rating
			String rating = i.nextLine(); //saving to rating variable

			//comparing rating against valid ratings
			if (rating.equals("G") ||
				rating.equals("PG") ||
				rating.equals("PG-13") ||
				rating.equals("R") ||
				rating.equals("NC-17") ||
				rating.equals("X"))
			{
				user.setRating(rating); //if valid input saving to variable
				shouldExit = true; //setting exit variable
			}else{ //begin else
				System.out.println("Invalid Input"); //if input is not valid
			} //end else
		} //end loop
	} //end method
} //end class