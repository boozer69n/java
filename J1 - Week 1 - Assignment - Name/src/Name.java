//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 1 - Assignment - Name
//#################################################################################################
//Create a Java program that prints out your name to the console and also displays it in a dialog
//box. Save, compile, and execute your program.
//#################################################################################################

import javax.swing.JOptionPane; //import ability for pop up

public class Name //defining class
{
	public static void main(String name[]) //begin main
	{
		System.out.println("Clinton Booze"); //printing name to console
		JOptionPane.showMessageDialog(null, "Clinton Booze"); //dialog box with name
			
	}
}