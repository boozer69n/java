//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 2 - Assignment - Movie
//#################################################################################################
//Create a class named TestMovie that creates three Movie instances with different values for
//name and rating and prints the information about each movie.
//#################################################################################################

import java.util.Scanner; //importing the ability to scan for input

public class TestMovie {
	public static void main(String[] args) { //begin main
		
		Movie movieOne = new Movie(); //creating instance for movie one
		Movie movieTwo = new Movie(); //creating instance for movie two
		Movie movieThree = new Movie(); //creating instance for movie three
		
		Scanner i = new Scanner(System.in); //create scanner object
		
		//running the methods for setting movie name and rating
		setMovie(movieOne, i);
		setRating(movieOne, i);
		setMovie(movieTwo, i);
		setRating(movieTwo, i);
		setMovie(movieThree, i);
		setRating(movieThree, i);
		
		i.close(); //closing scanner object
				
		//displaying movie info through display
		movieOne.display();
		movieTwo.display();
		movieThree.display();
	}
	
	//method for setting the movie name
	static void setMovie(Movie movieNumber, Scanner i)
	{
		String movieName;
		System.out.println(" "); //space for cleaner ui	
		System.out.print("Movie Name: "); //requesting user for movie name
		movieName = i.nextLine(); //saving to variable
		movieNumber.setMovieName(movieName); //sending to object
	}
	
	//method for setting the movie rating
	static void setRating(Movie movieNumber, Scanner i)
	{
		boolean shouldExit = false; //creating exit variable for loop
		while (!shouldExit){ //while loop for ratings
			System.out.println("Available Ratings: G, PG, PG-13, R, NC-17, X"); //displaying available ratings
			System.out.print("Movie Rating: "); //requesting user input for rating
			String rating = i.nextLine(); //saving to rating variable
		
			//comparing rating against valid ratings
			if (rating.equals("G") ||
				rating.equals("PG") ||
				rating.equals("PG-13") ||
				rating.equals("R") ||
				rating.equals("NC-17") ||
				rating.equals("X"))
			{
				movieNumber.setMovieRating(rating); //if valid input set rating
				shouldExit = true; //variable to exit loop
			}else{
				System.out.println("Invalid Input"); //if input is not valid
			}
		}
	}	
}
