//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 2 - Assignment - Movie
//#################################################################################################
//Create a class named Movie that holds a movie name and rating. Provide methods to get and set
//both the movie name and rating.
//#################################################################################################

public class Movie {
	private String movieName; //movie name variable
	private String movieRating; //movie rating variable
	
	public String getMovieName() { //will return movie name
		return movieName;
	}
	public void setMovieName(String movieName) { //saving received string to movie name variable
		this.movieName = movieName;
	}
	public String getMovieRating() { //will return movie rating
		return movieRating;
	}
	public void setMovieRating(String movieRating) { //saving received string to movie rating variable
		this.movieRating = movieRating;
	}
	public void display() { //displaying movie information
		System.out.println(" ");
		System.out.println("Movie: " + getMovieName() );
		System.out.println("Rating: " + getMovieRating() );

	}

}