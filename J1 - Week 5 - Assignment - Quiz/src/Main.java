/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Assignment - Quiz
###################################################################################################
a - Write an application that creates a quiz. The quiz should contain at least five questions
	about a hobby, popular music, astronomy, or any other personal interest. Each question
	should be a multiple- choice question with at least four answer options. When the user
	answers the question correctly, display a congratulatory message. If the user responds to
	a question incorrectly, display an appropriate message as well as the correct answer. At
	the end of the quiz, display the number of correct and incorrect answers, and the
	percentage of correct answers. Save the file as Quiz.java.
b - Modify the Quiz application so that the user is presented with each question continually
	until it is answered correctly. Remove the calculation for percentage of correct answers�
	all users will have 100% correct by the time they complete the application.
	Save the file as Quiz2.java.
###################################################################################################*/

import java.util.Scanner; //importing scanner

public class Main { //begin class
	public static void main( String[] args ){ //begin main
		Scanner i = new Scanner(System.in); //creating scanner object
		Quiz qOne = new Quiz(); //quiz 1 object
		Quiz2 qTwo = new Quiz2(); //quiz 2 object
		//displaying category of quiz
		System.out.println("I have been playing Minecraft lately so my questions");
		System.out.println("are all revolved around the game.");
		
		boolean shouldExit = false; //exit variable
		while(!shouldExit){ //begin loop
			//ui for choosing quiz
			System.out.println("");
			System.out.println("Please pick which you would like");
			System.out.println("1 - Quiz 1");
			System.out.println("    You will be asked 5 questions and only one chance to answer");
			System.out.println("    each question and your results will be displayed at the end");
			System.out.println("2 - Quiz 2");
			System.out.println("    You will be asked 5 questions and will be asked the question until you");
			System.out.println("    get the correct answer and your results will be printed at the end");
			System.out.println("3 - Exit");
			System.out.print("Selection: ");
			
			if(i.hasNextInt()){ //making sure an int is entered
				int userSelection = i.nextInt(); //saving int
				i.nextLine(); //removing return
				if(userSelection > 0 && userSelection < 4){ //checking for valid input
					switch(userSelection){ //begin switch
						case 1: //if 1 is entered
							qOne.run(i); //run quiz 1
							break; //end case
						case 2: //if 2 is entered
							qTwo.run(i); //run quiz 2
							break; //end case
						case 3: //if 3 is entered
							shouldExit = true; //exit variable
							System.out.println("Bye o/"); //waving bye
							break; //end case
						default: //for all other inputs
							System.out.println("Invalid Input"); //invalid entry
					} //end switch
				} //end if
			}else{ //begin else
				System.out.println("Invalid Input"); //invalid entry
			} //end else
		} //end loop
		i.close(); //close scanner
	} //end main
} //end class
