/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Assignment - Quiz
###################################################################################################
a - Write an application that creates a quiz. The quiz should contain at least five questions
	about a hobby, popular music, astronomy, or any other personal interest. Each question
	should be a multiple- choice question with at least four answer options. When the user
	answers the question correctly, display a congratulatory message. If the user responds to
	a question incorrectly, display an appropriate message as well as the correct answer. At
	the end of the quiz, display the number of correct and incorrect answers, and the
	percentage of correct answers. Save the file as Quiz.java.
b - Modify the Quiz application so that the user is presented with each question continually
	until it is answered correctly. Remove the calculation for percentage of correct answers�
	all users will have 100% correct by the time they complete the application.
	Save the file as Quiz2.java.
###################################################################################################*/

public class Questions { //begin class
	private int correct; //number correct variable
	private int tries; //number of attempts
	//setting the questions and answers	
	private String[] qOne = {"How many bars does it take to make a block?",
							 "A) 7",
							 "B) 8",
							 "C) 9",
							 "D) 10",
							 "C"}; //answer
	private String[] qTwo = {"What level is a max level enchant?",
							 "A) 50",
							 "B) 60",
							 "C) 30",
			 				 "D) 10",
			 				 "C"}; //answer
	private String[] qThree = {"What is the build limit height?",
			 				   "A) 64",
			 				   "B) 128",
			 				   "C) 256",
			 				   "D) 512",
			 				   "C"}; //answer
	private String[] qFour = {"What type of pick does it take to mine diamonds?",
			 				  "A) Wood",
			 				  "B) Stone",
			 				  "C) Iron",
			 				  "D) Diamond",
			 				  "C"}; //answer
	private String[] qFive = {"How many colors can sheep be dyed?",
							  "A) 8",
							  "B) 20",
							  "C) 16",
							  "D) 24",
							  "C"}; //answer
	
	public Questions(int correct, int tries){ //constructor
		this.correct = correct; //setting correct
		this.tries = tries; //setting tries
	} //end constructor
	
	public void getDisplay(int q){ //picks which question to display
		if(q == 1){ //begin if
			display(qOne); //displays question 1
		}else if(q == 2){  //begin if
			display(qTwo); //displays question 2
		}else if(q == 3){ //begin if
			display(qThree); //displays question 3
		}else if(q == 4){ //begin if
			display(qFour); //displays question 4
		}else if(q == 5){ //begin if
			display(qFive); //displays question 5
		}else{ //begin else
			System.out.println("Invalid Question Number"); //if incorrect question asked
		} //end else
	} //end method
	
	public void display(String[] a){ //displaying question
		System.out.println(""); //for display
		for(int i = 0; i < 5; i++){ //begin loop
			System.out.println(a[i]); //display the array
		} //end loop
		System.out.print("Your Answer: "); //requesting your answer
	} //end method
	
	public int getCorrect(){ //begin method
		return this.correct; //returning number correct
	} //end method
	
	public void setCorrect(){ //begin method
		this.correct++; //adding 1 to the number correct
	} //end method
	
	public void setTries(){ //begin method
		this.tries++; //adding 1 to number of tries
	} //end method
	
	public int getTries(){ //begin method
		return this.tries; //returning the number of tries
	} //end method
	
	public String getAnswer(int arr){ //picking which answer to return
		String answer = null;
		if(arr == 1){  //begin if
			answer = retAnswer(qOne); //saving answer
		}else if(arr == 2){  //begin if
			answer = retAnswer(qTwo); //saving answer
		}else if(arr == 3){  //begin if
			answer = retAnswer(qThree); //saving answer
		}else if(arr == 4){  //begin if
			answer = retAnswer(qFour); //saving answer
		}else if(arr == 5){  //begin if
			answer = retAnswer(qFive); //saving answer
		}else{  //begin else
			System.out.println("Invalid Question Number");
		} //end else
		return answer; //returning answer
	} //end method
	
	public String retAnswer(String[] a){ //begin method
		String answer = a[5]; //saving answer
		return answer; //returns answer for requested question
	} //end method
		
} //end class
