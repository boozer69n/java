/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Assignment - Quiz
###################################################################################################
a - Write an application that creates a quiz. The quiz should contain at least five questions
	about a hobby, popular music, astronomy, or any other personal interest. Each question
	should be a multiple- choice question with at least four answer options. When the user
	answers the question correctly, display a congratulatory message. If the user responds to
	a question incorrectly, display an appropriate message as well as the correct answer. At
	the end of the quiz, display the number of correct and incorrect answers, and the
	percentage of correct answers. Save the file as Quiz.java.
b - Modify the Quiz application so that the user is presented with each question continually
	until it is answered correctly. Remove the calculation for percentage of correct answers�
	all users will have 100% correct by the time they complete the application.
	Save the file as Quiz2.java.
###################################################################################################*/

import java.text.DecimalFormat;
import java.util.Scanner;

public class Quiz2 { //begin class
	public void run(Scanner i){ //begin method
		DecimalFormat fmt = new DecimalFormat("0"); //creating format object
		Questions q = new Questions(0,0); //constructing object
		
		for(int j = 1; j < 6; j++){ //begin loop of questions
			q.getDisplay(j); //display question
			String userAnswer = i.nextLine(); //save answer
			boolean shouldExit = false; //exit variable
			while(!shouldExit){ //begin loop
				if(userAnswer.equalsIgnoreCase(q.getAnswer(j))){ //checking answer
					//congratulatory message
					System.out.println("                   !! Correct !!                   ");
					System.out.println("                   __ooooooooo__                   ");
					System.out.println("              oOOOOOOOOOOOOOOOOOOOOOo              ");
					System.out.println("          oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo          ");
					System.out.println("       oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo       ");
					System.out.println("     oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo     ");
					System.out.println("   oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo   ");
					System.out.println("  oOOOOOOOOOOO*  *OOOOOOOOOOOOOO*  *OOOOOOOOOOOOo  ");
					System.out.println(" oOOOOOOOOOOO      OOOOOOOOOOOO      OOOOOOOOOOOOo ");
					System.out.println(" oOOOOOOOOOOOOo  oOOOOOOOOOOOOOOo  oOOOOOOOOOOOOOo ");
					System.out.println("oOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOo");
					System.out.println("oOOOO     OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO     OOOOo");
					System.out.println("oOOOOOO OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO OOOOOOo");
					System.out.println(" *OOOOO  OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO  OOOOO* ");
					System.out.println(" *OOOOOO  *OOOOOOOOOOOOOOOOOOOOOOOOOOOOO*  OOOOOO* ");
					System.out.println("  *OOOOOO  *OOOOOOOOOOOOOOOOOOOOOOOOOOO*  OOOOOO*  ");
					System.out.println("   *OOOOOOo  *OOOOOOOOOOOOOOOOOOOOOOO*  oOOOOOO*   ");
					System.out.println("     *OOOOOOOo  *OOOOOOOOOOOOOOOOO*  oOOOOOOO*     ");
					System.out.println("       *OOOOOOOOo  *OOOOOOOOOOO*  oOOOOOOOO*       ");
					System.out.println("          *OOOOOOOOo           oOOOOOOOO*          ");
					System.out.println("              *OOOOOOOOOOOOOOOOOOOOO*              ");
					System.out.println("                   **ooooooooo**                   ");
					
					q.setCorrect(); //adding to correct
					q.setTries(); //adding to tries
					shouldExit = true; //exit variable
				}else{ //begin else
					System.out.println("Wrong"); //display wrong
					q.setTries(); //add to tries
					System.out.print("Try Again: "); //requests a new answer
					userAnswer = i.nextLine(); //saves answer
				} //end else
			} //end loop
		} //end loop
		float correct = q.getCorrect(); //saving the int as a float
		String correctPercentage = fmt.format((correct / 5) * 100); //displaying in correct format
		System.out.println(""); //for display
		System.out.println("You got " + fmt.format(q.getCorrect()) + " correct"); //displays number correct
		System.out.println("That is a " + correctPercentage + "%"); //displays your correct %
		System.out.println(""); //for display
		float tries = q.getTries(); //saving int as float
		String triesPercentage = fmt.format((5 / tries) * 100); //displaying in correct format
		System.out.println("You guessed " + q.getTries() + " times"); //displays number of tries
		System.out.println("That is " + triesPercentage + "% guessed correctly"); //displays % of tries correct
	} //end method
} //end class