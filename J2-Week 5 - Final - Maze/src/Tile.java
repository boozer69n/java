import java.awt.Color;
import java.awt.Graphics;


public class Tile{
	
	private int roomSize;
	private int hallwaySize;
	private int wallThickness;
	private int x;
	private int y;	
	private boolean north;
	private boolean south;
	private boolean east;
	private boolean west;	
	
	public Tile(int x, int y, boolean n, boolean s, boolean e, boolean w){
		this.x = x;
		this.y = y;
		this.north = n;
		this.south = s;
		this.east = e;
		this.west = w;
		this.setRoomSize(40);
	} //Tile
	
	/**
	 * return the size of the room/tile
	 * @return
	 */
	public int getRoomSize(){
		return roomSize;
	} //getRoomSize
	
	/**
	 * sets the numbers based off of the size of the tiles
	 * @param roomSize
	 */
	private void setRoomSize(int roomSize) {
		this.roomSize = roomSize;
		this.hallwaySize = (int)Math.floor(this.roomSize / 3);
		this.wallThickness = (int) Math.floor(this.roomSize / 8);
	} //setRoomSize
	
	/**
	 * setting the x location of the tile
	 * @param x
	 */
	public void setX(int x){
		this.x = x;
	} //setX
	
	/**
	 * setting the y location of the tile
	 * @param y
	 */
	public void setY(int y){
		this.y = y;
	} //setY
	
	/**
	 * return the size of the hallway
	 * @return
	 */
	public int getHallwaySize(){
		return this.hallwaySize;
	} //getHallwaySize
	
	/**
	 * return the thickness of the walls
	 * @return
	 */
	public int getWallThickness(){
		return this.wallThickness;
	} //getWallThickness

	//set and get whether the hallway is open
	public boolean isNorth() { return north; }
	public void setNorth(boolean north) { this.north = north; }
	public boolean isSouth() { return south; }
	public void setSouth(boolean south) { this.south = south; }
	public boolean isEast() { return east; }
	public void setEast(boolean east) { this.east = east; }
	public boolean isWest() { return west; }
	public void setWest(boolean west) { this.west = west; }
	
	//get the location of the tile
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	
	/**
	 * display the tile
	 * @param g
	 */
	public void paintComponent(Graphics g){
		//border
		g.setColor(Color.ORANGE);
		g.fillRect(x, y, this.roomSize, this.roomSize);
		
		//hollowing it out
		g.setColor(Color.BLACK);
		g.fillRect(x + this.wallThickness, y + this.wallThickness, this.roomSize - (2*this.wallThickness), this.roomSize - (2*this.wallThickness));
		
		if(this.north){
			g.fillRect(x + ((int)Math.floor(this.roomSize / 2) - (int)Math.floor(this.hallwaySize / 2)),
					   y,
					   this.hallwaySize,
					   this.wallThickness);
		}
		if(this.south){
			g.fillRect(x + ((int)Math.floor(this.roomSize / 2) - (int)Math.floor(this.hallwaySize / 2)),		
					   y + (this.roomSize - this.wallThickness),
					   this.hallwaySize,
					   this.wallThickness);
		}
		if(this.east){
			g.fillRect(x + (this.roomSize - this.wallThickness),		
					   y + ((int)Math.floor(this.roomSize / 2) - (int)Math.floor(this.hallwaySize / 2)),
					   this.wallThickness,
					   this.hallwaySize);
		}
		if(this.west){
			g.fillRect(x ,
					   y + ((int)Math.floor(this.roomSize / 2) - (int)Math.floor(this.hallwaySize / 2)),
					   this.wallThickness,
					   this.hallwaySize);
		}
	} //paintComponent
} //Tile