import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


//fixes
//add in map random tiles to remove blank ones
//moving

public class Game implements KeyListener {
	private static JFrame frame = new JFrame();
	private static MazeMap mazeMap;
	private static Player player;
	private static Drawable draw;

	/**
	 * Launch the application.
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		gui();
	}
	
	/**
	 * displaying the instructions for the game
	 */
	public static void instructions(){
		String instructions = "<html><b>Objective:</b>" +
							  "<p>To get your player to the bottom right tile " +
							  "in the least amount of moves possible. " +
							  "Every <i>5</i> moves will cause the maze to randomize " + 
							  "itself again.</p>" +
							  "<b>Movement:</b>" +
							  "<p>You can use the <i>North, South, East or West</i> buttons. " +
							  "As an alternative you can use <i>WASD</i> or the <i>arrows</i>.</p>";
		
		JLabel label = new JLabel(instructions);
		label.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		JOptionPane.showMessageDialog(frame, label);		
	}
	
	/**
	 * displays the game
	 * @throws Exception
	 */
	public static void gui() throws Exception{
		frame.addKeyListener(new Game());
		player = new Player(10, 10, 40);
		mazeMap = new MazeMap();		
		
		//setting frame info
		frame.setTitle("Random Maze          by: Clinton Booze");
		frame.setResizable(false);		
		frame.setBounds(0, 0, 600, 425);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(new Dimension(600, 425));
		frame.setPreferredSize(new Dimension(600, 425));
		frame.setFocusable(true);
		frame.getContentPane().setLayout(null);
		frame.pack();
		
		
		//adding buttons
		JButton btnNorth = new JButton("North");
		JButton btnSouth = new JButton("South");
		JButton btnEast = new JButton("East");
		JButton btnWest = new JButton("West");
		JButton btnInstructions = new JButton("Instructions");
		
		//button listeners
		btnNorth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.move(mazeMap.getMazeArray(), 1);
				mapCheck();
				frame.repaint();
				try {
					winCheck();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frame.requestFocus();
			}
		});	
		
		btnSouth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.move(mazeMap.getMazeArray(), 2);
				mapCheck();
				frame.repaint();
				try {
					winCheck();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frame.requestFocus();
			}
		});
		
		btnEast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.move(mazeMap.getMazeArray(), 3);
				mapCheck();
				frame.repaint();
				try {
					winCheck();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frame.requestFocus();
			}
		});		
		
		btnWest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.move(mazeMap.getMazeArray(), 4);
				mapCheck();
				frame.repaint();
				try {
					winCheck();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frame.requestFocus();				
			}
		});

		btnInstructions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				instructions();
				frame.requestFocus();
			}
		});
		
		//setting button location and info
		btnNorth.setBounds(464, 50, 70, 22);
		btnSouth.setBounds(464, 110, 70, 22);
		btnEast.setBounds(506, 80, 70, 22);
		btnWest.setBounds(426, 80, 70, 22);
		btnInstructions.setBounds(426, 175, 150, 23);
		
		//drawing the items
		draw = new Drawable(mazeMap, player);
		draw.setBackground(Color.PINK);
		draw.setBounds(0, 0, 600, 425);
		frame.getContentPane().add(draw);
		
		//adding buttons
		frame.getContentPane().add(btnNorth);
		frame.getContentPane().add(btnSouth);
		frame.getContentPane().add(btnEast);
		frame.getContentPane().add(btnWest);
		frame.getContentPane().add(btnInstructions);
		
		//updating picture
		frame.repaint();
		draw.repaint();
	} //gui
	
	/**
	 * checking the map to see if 5 moves were done
	 */
	public static void mapCheck(){
		if(player.getMovesForGenerate() == 5){
			mazeMap.generateMap();
			player.setMovesForGenerate(0);
		}
	} //mapCheck
	
	public static void winCheck() throws Exception{
		if(player.winLocation()){
			player.playerReset();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
	} //keyPressed
	
	@Override
	public void keyReleased(KeyEvent e) {
		int c = e.getKeyCode();
		if(c == KeyEvent.VK_UP || c == KeyEvent.VK_W){
			player.move(mazeMap.getMazeArray(), 1);
			mapCheck();
			draw.repaint();
			try {
				winCheck();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		if(c == KeyEvent.VK_DOWN || c == KeyEvent.VK_S){
			player.move(mazeMap.getMazeArray(), 2); 
			mapCheck();
			draw.repaint();
			try {
				winCheck();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		if(c == KeyEvent.VK_RIGHT || c == KeyEvent.VK_D){
			player.move(mazeMap.getMazeArray(), 3);
			mapCheck();
			draw.repaint();
			try {
				winCheck();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		if(c == KeyEvent.VK_LEFT || c == KeyEvent.VK_A){
			player.move(mazeMap.getMazeArray(), 4);
			mapCheck();
			draw.repaint();
			try {
				winCheck();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	} //keyReleased
	
	@Override
	public void keyTyped(KeyEvent e) {
	} //keyTyped
}