import java.awt.Graphics;
import javax.swing.JComponent;


public class Drawable extends JComponent {
	private MazeMap map;
	private Player player;
	
	public Drawable(MazeMap map, Player player) {
		this.map = map;
		this.player = player;
	} //Drawable
	
	@Override
	public void paintComponent(Graphics g) {
		map.paintComponent(g);
		player.paintComponent(g);
	} //paintComponent
} //Drawable