import java.awt.Graphics;
import java.io.Console;
import java.util.Random;


public class MazeMap{
	
	//containing all the tiles for the maze
	private Tile[][] mazeTiles = new Tile[10][10];
	
	/**
	 * constructor just generates a map
	 */
	public MazeMap(){
		generateMap();
	} //constructor
	
	/**
	 * will randomly regenerate a new map
	 */
	public void generateMap(){
		boolean success = false;
		while (!success){
			this.randomMap();
			this.fixTileConnections();
			success = this.ensureOpening();
			if (success) this.fixTileLocations();
		}
	} //generateMap
	
	/**
	 * @return
	 * 		returning the 2 dimensional array of tiles
	 */
	public Tile[][] getMazeArray(){
		return this.mazeTiles;
	} //getMazeArray
	
	/**
	 * displaying the maze tiles
	 */
	public void paintComponent(Graphics g){
		int x, y;
		for(y = 0; y < 10; y++){
			for(x = 0; x < 10; x++){
				mazeTiles[x][y].paintComponent(g);
			}
		}
	} //paintComponent
	
	/**
	 * checks to ensure that all tiles have at least one opening
	 * and if it doesn't a new tile is generated for that location
	 */
	private boolean ensureOpening(){
		int tileCount = 0;
		int x, y;
		int cycles = 0;
		boolean success = false;
		while(tileCount != 100){
			cycles++;
			for(y = 0; y < 10; y++){
				for(x = 0; x < 10; x++){
					if(this.mazeTiles[x][y].isNorth() ||
					    this.mazeTiles[x][y].isSouth() || 
					    this.mazeTiles[x][y].isEast() || 
					    this.mazeTiles[x][y].isWest()){
						tileCount++;
					}else{						
						this.mazeTiles[x][y] = randomTile();
						fixTileConnections();
					}
				}
			}
			if(cycles >= 25) break;
			if(tileCount != 100) tileCount = 0;
			else success = true; 
		}		
		
		return success;
	} //ensureOpening
	
	/**
	 * populating a 2 dimensional array with random tiles
	 */
	private void randomMap(){
		int x, y;
		for(y = 0; y < 10; y++){
			for(x = 0; x < 10; x++){
				mazeTiles[x][y] = randomTile();
			}
		}		
	} //randomMap
	
	/**
	 * returns a random tile with 0-4 hallways open
	 * @return
	 */
	private Tile randomTile(){
		Random random = new Random();
		Tile tempTile = new Tile (0, 0, random.nextBoolean(),
				  						random.nextBoolean(),
				  						random.nextBoolean(),
				  						random.nextBoolean());
		if(!tempTile.isNorth() && !tempTile.isSouth() && !tempTile.isEast() && !tempTile.isWest()){
			int temp = random.nextInt(4);
			switch(temp){
			case 0:
				tempTile.setNorth(true);
			case 1:
				tempTile.setSouth(true);
			case 2:
				tempTile.setEast(true);
			default:
				tempTile.setWest(true);
			}
		}
		return tempTile;
	} //randomTile
	
	/**
	 * fixing the connections between tiles to ensure that
	 * if a tile has an opening it isn't facing a wall in
	 * the adjoining tile
	 */
	private void fixTileConnections(){
		int x, y;		
		for(y = 0; y < 10; y++){
			for(x = 0; x < 10; x++){
				this.mazeTiles[x][y].setNorth(tileCompare(x, y, 1));
				this.mazeTiles[x][y].setSouth(tileCompare(x, y, 2));
				this.mazeTiles[x][y].setEast(tileCompare(x, y, 3));
				this.mazeTiles[x][y].setWest(tileCompare(x, y, 4));
			}
		}
	} //fixTileConnections
	
	/**
	 * @param x = x location of the tile to be modified
	 * @param y = y location of the tile to be modified
	 * @param direction = the direction of the tile to be modified
	 * @return
	 * 		returning whether or not an opening should be placed
	 * 		within the tile based off of the adjoining tile and
	 * 		if it has an opening or not
	 */
	private boolean tileCompare(int x, int y, int direction){
		int dirX, dirY;
		//north=1, south=2, east=3, west=4
		switch(direction){
		case 1:
			dirX = x;
			dirY = y-1;
			if(isEdge(dirX, dirY)) return false;
			if(this.mazeTiles[dirX][dirY].isSouth()) return true;
			return false;
		case 2:
			dirX = x;
			dirY = y+1;
			if(isEdge(dirX, dirY)) return false;
			if(this.mazeTiles[dirX][dirY].isNorth()) return true;
			return false;
		case 3:
			dirX = x+1;
			dirY = y;
			if(isEdge(dirX, dirY)) return false;
			if(this.mazeTiles[dirX][dirY].isWest()) return true;
			return false;
		default:
			dirX = x-1;
			dirY = y;
			if(isEdge(dirX, dirY)) return false;
			if(this.mazeTiles[dirX][dirY].isEast()) return true;
			return false;
		}
	} //tileCompare
	
	/**
	 * @param x = x location of the tile being checked
	 * @param y = y location of the tile being checked
	 * @return
	 * 		returns whether or not the tile being checked is
	 * 		on the edge of the map
	 */
	private boolean isEdge(int x, int y){
		if(x < 0 || y < 0 || x > 9 || y > 9) return true;
		else return false;		
	} //isEdge
	
	/**
	 * adjusts the tile locations based off of the room size
	 * placing the tiles in the correct location within the maze
	 */
	private void fixTileLocations() {
		int xloc, yloc;
		for(yloc = 0; yloc < 10; yloc++){
			for(xloc = 0; xloc < 10; xloc++){
				this.mazeTiles[xloc][yloc].setX(xloc * this.mazeTiles[xloc][yloc].getRoomSize());
				this.mazeTiles[xloc][yloc].setY(yloc * this.mazeTiles[xloc][yloc].getRoomSize());
			}
		}
	} //fixTileLocations
		
} //MazeMap
