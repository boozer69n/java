import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Database {
	
	private int[] topScores = new int[10];
	
	/**
	 * returns an array of the top ten scores after pulling it from the database
	 * @return
	 * @throws Exception
	 */
	public int[] getDatabaseTopTen() throws Exception{
		//accessing driver from jar file
		Class.forName("com.mysql.jdbc.Driver");
		
		//creating variable for the connection called con
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/maze","javatwo","javatwo");
		//jdbc:mysql://localhost:3306/maze --> this is the database
		//javatwo is the database user
		//javatwo is the password
		
		//here we create a query
		PreparedStatement statement = con.prepareStatement("select * from topscores");
		
		//creating variable to execute query
		ResultSet result = statement.executeQuery();
		
		result.next();
		for(int i = 1; i < 11; i++){
			topScores[i-1] = result.getInt(i);
		}
		con.close();
		return topScores;
	} //getDatabaseTopTen
	
	/**
	 * takes in an array and uploads it to the database
	 * @param topTen
	 * @throws Exception
	 */
	public void setDatabaseTopTen(int[] topTen) throws Exception{
		//accessing driver from jar file
		Class.forName("com.mysql.jdbc.Driver");
		
		//creating variable for the connection called con
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/maze","javatwo","javatwo");
		//jdbc:mysql://localhost:3306/maze --> this is the database
		//javatwo is the database user
		//javatwo is the password
		
		//here we create a statement and run it
		PreparedStatement statement = con.prepareStatement("UPDATE `topscores` SET `top01`="+topTen[0]+",`top02`="+topTen[1]+",`top03`="+topTen[2]+",`top04`="+topTen[3]+",`top05`="+topTen[4]+",`top06`="+topTen[5]+",`top07`="+topTen[6]+",`top08`="+topTen[7]+",`top09`="+topTen[8]+",`top10`="+topTen[9]+" WHERE 1");
		statement.execute();

		con.close();
	} //setDatabaseTopTen
}
