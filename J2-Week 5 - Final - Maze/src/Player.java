import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Arrays;


public class Player{
	
	//location of the player
	private int xLocation, yLocation;
	private int roomSize;
	private int currentMoves = 0;
	private int movesForGenerate = 0;
	private int[] topTen = new int[10];
	private Database database = new Database();
	
	
	public Player(int x, int y, int roomSize) throws Exception {
		this.xLocation = x;
		this.yLocation = y;
		this.roomSize = roomSize;
		this.topTen = database.getDatabaseTopTen();
	} //constructor
	
	/**
	 * displaying the player components
	 * @param g
	 */
	public void paintComponent(Graphics g){
		displayPlayer(g);
		displayTopScores(g);
		displayCurrentMoves(g);
	} //paintComponent
	
	/**
	 * used to reset the player to the beginning if they have won
	 * @throws Exception 
	 */
	public void playerReset() throws Exception{
		addScore(this.currentMoves);
		this.xLocation = 10;
		this.yLocation = 10;
		this.currentMoves = 0;
		this.movesForGenerate = 0;
		database.setDatabaseTopTen(topTen);
	} //playerReset
	
	/**
	 * displaying the top scores
	 * @param g
	 */
	private void displayTopScores(Graphics g){
		g.setColor(Color.BLACK);
		int startX = 500;
		int startY = 245;
		
		g.setFont(new Font("Century Gothic", Font.BOLD, 14));
		g.drawString("TOP SCORES", startX, startY-15);
		
		g.setFont(new Font("Century Gothic", Font.PLAIN, 14));
		for(int i = 0; i < topTen.length; i++){
			g.drawString(String.valueOf(topTen[i]), startX, startY);
			startY += 15;
		}		
	} //displayTopScores
	
	/**
	 * displaying the player
	 * @param g
	 */
	private void displayPlayer(Graphics g){
		g.setColor(Color.CYAN);
		g.fillOval(this.xLocation,
				   this.yLocation,
				   (int)Math.floor(this.roomSize/2),
				   (int)Math.floor(this.roomSize/2));
	} //displayPlayer
	
	/**
	 * displaying the current moves of the current game
	 * @param g
	 */
	private void displayCurrentMoves(Graphics g){
		int startX = 450;
		int startY = 20;
		g.setColor(Color.BLACK);
		g.setFont(new Font("Century Gothic", Font.PLAIN, 14));
		g.drawString("Current Moves: " + this.currentMoves, startX, startY);
	} //displayCurrentMoves
	
	/**
	 * saves the top scores to the database
	 * @throws Exception
	 */
	public void saveTopScores() throws Exception{
		database.setDatabaseTopTen(this.topTen);
	} //saveTopScores
	
	/**
	 * moves the player the desired direction
	 * @param maze
	 * @param direction
	 */
	public void move(Tile[][] maze, int direction){
		int xTile = (this.xLocation - 10) / this.roomSize;
		int yTile = (this.yLocation - 10) / this.roomSize;
		
		switch(direction){
		case 1: //north
			if (maze[xTile][yTile].isNorth()){
				this.moveY(-this.roomSize);
				this.currentMoves++;
				this.movesForGenerate++;
			}
			break;
		case 2: //south
			if (maze[xTile][yTile].isSouth()){
				this.moveY(this.roomSize);
				this.currentMoves++;
				this.movesForGenerate++;
			}
			break;
		case 3: //east
			if (maze[xTile][yTile].isEast()){
				this.moveX(this.roomSize);
				this.currentMoves++;
				this.movesForGenerate++;
			}
			break;
		case 4: //west
			if (maze[xTile][yTile].isWest()){
				this.moveX(-this.roomSize);
				this.currentMoves++;
				this.movesForGenerate++;
			}
			break;
		}
	} //move
	
	/**
	 * add a score to the top score array if it is higher
	 * @param score
	 */
	public void addScore(int score){
		if(score < this.topTen[9]){
			this.topTen[9] = score;
			Arrays.sort(this.topTen);
		}
	} //addScore
	
	/**
	 * return the top scores array
	 * @return
	 */
	public int[] getTopScores(){
		return this.topTen;
	} //getTopScores
	
	/**
	 * returning whether or not the player has finished
	 * @return
	 */
	public boolean winLocation(){
		int winXLocation = 370;
		int winYLocation = 370;
		if(winXLocation == this.xLocation && winYLocation == this.yLocation){
			return true;
		}
		return false;
	} //winLocation
	
	/**
	 * move the player along the x axis
	 * @param value
	 */
	private void moveX(int value){
		int direction = 1;
		if(value < 0) direction = -1;

		try{
			for(int i = 0; i < Math.abs(value); i++){
				this.xLocation += direction;
				Thread.sleep(2);
			}
		}catch (InterruptedException e){
			e.printStackTrace();
		}
	} //moveX
	
	/**
	 * move the player along the y axis
	 * @param value
	 */
	private void moveY(int value){
		int direction = 1;
		if(value < 0) direction = -1;

		try{
			for(int i = 0; i < Math.abs(value); i++){
				this.yLocation += direction;
				Thread.sleep(2);
			}
		}catch (InterruptedException e){
			e.printStackTrace();
		}
	} //moveY

	/**
	 * return the current number of moves
	 * @return
	 */
	public int getCurrentMoves(){
		return this.currentMoves;
	} //getCurrentMoves

	/**
	 * set the current moves
	 * @param currentMoves
	 */
	public void setCurrentMoves(int currentMoves){
		this.currentMoves = currentMoves;
	} //setCurrentMoves

	/**
	 * get the number of moves used for map regeneration
	 * @return
	 */
	public int getMovesForGenerate(){
		return movesForGenerate;
	} //getMovesForGenerate

	/**
	 * set the number of moves that are used for map generation
	 * @param movesForGenerate
	 */
	public void setMovesForGenerate(int movesForGenerate){
		this.movesForGenerate = movesForGenerate;
	} //setMovesForGenerate
}
