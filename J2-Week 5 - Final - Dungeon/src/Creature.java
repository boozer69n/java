import java.util.Random;

public class Creature {
	Random rand = new Random();
	private String name;
	private String className;
	private int health;
	private int maxHealth;
	private int strength;
	private int endurance;
	private int dexterity;
	private double hitRate;
	private int level;
	private int experience;
	
	/**
	 * 
	 * @param name = name of creature
	 * @param cName = class name (rogue, warrior)
	 * @param health = life of the creature
	 * @param str = strength of the creature
	 * @param def = defense of the creature
	 * @param dex = dexterity of the creature
	 * @param hit = hit rating of the creature
	 * @param lvl = level of the creature
	 * @param exp = experience of the creature
	 */
	public Creature(String name, int health, int str, int end, int dex, double hit, int lvl, String cName){
		this.setName(name);
		this.setClassName(cName);
		this.setHealth(health);
		this.maxHealth = health;
		this.setStrength(str);
		this.setEndurance(end);
		this.setDexterity(dex);
		this.setHitRate(hit);
		this.setLevel(lvl);
		this.experience = 0;		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the health
	 */
	public int getHealth() {
		return health;
	}

	/**
	 * @param health the health to set
	 */
	public void setHealth(int health) {
		this.health = health;
	}

	/**
	 * @return the strength
	 */
	public double getStrength() {
		return strength;
	}

	/**
	 * @param strength the strength to set
	 */
	public void setStrength(int strength) {
		this.strength = strength;
	}

	/**
	 * @return the dexterity
	 */
	public double getDexterity() {
		return dexterity;
	}

	/**
	 * @param dexterity the dexterity to set
	 */
	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}

	/**
	 * @return the hitRate
	 */
	public double getHitRate() {
		return hitRate;
	}

	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(double hitRate) {
		this.hitRate = hitRate;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the experience
	 */
	public int getExperience() {
		return experience;
	}

	/**
	 * @param experience the experience to set
	 */
	public void setExperience(int experience) {
		this.experience = experience;
	}
	
	public int expToLevel(int level){
	    return 128 * level * level;
	}
	
	/**
	 * @return the endurance
	 */
	public int getEndurance() {
		return endurance;
	}

	/**
	 * @param endurance the endurance to set
	 */
	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}
	
	boolean levelUp(){
	    if(this.experience >= expToLevel(this.level+1)){
	        ++level;
	 
	        int healthBoost = 0;
	        int strBoost = 0;
	        int endBoost = 0;
	        int dexBoost = 0;
	 
	        if(level % 3 == 0){
	            healthBoost = 10 + (rand.nextInt() % 4) + this.endurance / 4;
	        }else{
	            healthBoost = this.endurance / 4;
	        }

	        if(rand.nextInt() % 2 == 0){ strBoost = 2;
	        }else{ strBoost = 1;}
	        
	        if(rand.nextInt() % 2 == 0){ endBoost = 2;
	        }else{ endBoost = 1;}
	        
	        if(rand.nextInt() % 2 == 0){ dexBoost = 2;
	        }else{ dexBoost = 1;}
	        
	        this.maxHealth += healthBoost;
	        this.strength += strBoost;
	        this.endurance += endBoost;
	        this.dexterity += dexBoost;
	 
	        System.out.printf("%s leveled up to %d\n", this.name, this.level);
	        System.out.printf("Health increased by: %d to %d", healthBoost, this.maxHealth);
	        System.out.printf("Strength increased by: %d to %d", strBoost, this.strength);
	        System.out.printf("Endurance increased by: %d to %d", endBoost, this.endurance);
	        System.out.printf("Dexterity increased by: %d to %d", dexBoost, this.dexterity);
	        System.out.println("=======================================");	 
	        return true;
	    }
	    return false;
	}



}
