import java.util.ArrayList;
import java.util.Scanner;


public class Dialogue {
	Scanner sc = new Scanner(System.in);
	private String description;
	private ArrayList< String > choices = new ArrayList< String >();
	
	public Dialogue(String description, ArrayList<String> choices){
		this.description = description;
		this.choices = choices;
	}
	
	public int selection(){
		int userInput = -1;
		System.out.println(this.description);
		
		for(int i = 0; i < choices.size(); i++){
			System.out.print(i+1);
			System.out.print(": ");
			System.out.println(this.choices.get(i));
		}
		
		while(true){
			userInput = sc.nextInt();
			if(userInput >= 0 && userInput <= choices.size()){
				return userInput;
			}
		}
	}
}
