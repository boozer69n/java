import java.util.ArrayList;


public class Zone {
	private Dialogue dialogue;
	private Inventory items;
	private ArrayList< Creature > creatures = new ArrayList< Creature >();
	
	public Zone(Dialogue dialogue, Inventory items, ArrayList<Creature> creatures){
		this.dialogue = dialogue;
		this.items = items;
		this.creatures = creatures;
	}
	
	void find(Creature player){
		System.out.println("You found: ");
		this.items.print();
		player.inventory.add();
		this.items.clear;
	}
	
}
