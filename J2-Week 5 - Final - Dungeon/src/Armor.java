
public class Armor extends Item{
	private double defense;
	private Slot mySlot;

	public Armor(String name, String desc, double def, Slot slot) {
		super(name, desc);
		this.setDefense(def);
		this.setMySlot(slot);
	}

	/**
	 * @return the defense
	 */
	public double getDefense() {
		return defense;
	}

	/**
	 * @param defense the defense to set
	 */
	public void setDefense(double defense) {
		this.defense = defense;
	}

	/**
	 * @return the mySlot
	 */
	public Slot getMySlot() {
		return mySlot;
	}

	/**
	 * @param mySlot the mySlot to set
	 */
	public void setMySlot(Slot mySlot) {
		this.mySlot = mySlot;
	}
}