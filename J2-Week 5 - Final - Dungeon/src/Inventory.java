import java.util.ArrayList;


public class Inventory {
	private  ArrayList< Item > inv = new ArrayList< Item >();
	private int money;
		
	/**
	 * 
	 * @param myItem
	 * 		Adds an item to the inventory
	 */
	public void addItem(Item myItem){
		this.inv.add(myItem);
	}
	
	/**
	 * 
	 * @param myItem
	 * 		Removes an item from the inventory
	 */
	public void removeItem(Item myItem){
		int index = inv.indexOf(myItem);
		this.inv.remove(index);
	}

	/**
	 * @return the money
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * @param money the money to set
	 */
	public void setMoney(int money) {
		this.money = money;
	}	

}


