
public class Weapon extends Item{
	private double hitRate;
	private double damage;
	
	public Weapon(String name, String desc, double hit, double dmg){
		super(name, desc);
		this.setHitRate(hit);
		this.setDamage(dmg);
	}

	/**
	 * @return the hitRate
	 */
	public double getHitRate() {
		return hitRate;
	}

	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(double hitRate) {
		this.hitRate = hitRate;
	}

	/**
	 * @return the damage
	 */
	public double getDamage() {
		return damage;
	}

	/**
	 * @param damage the damage to set
	 */
	public void setDamage(double damage) {
		this.damage = damage;
	}

}