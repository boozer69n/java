import java.util.ArrayList;



public class Database {

	
	private void buildCreatures(ArrayList<Creature> Zone){
	    // Creature(Name, Health, Str, End, Dex, Hit Rate, Level)
	    Zone.add(new Creature("Rat", 8, 8, 8, 12, 2.0, 1, ""));
	}
	 
	private void buildItems(ArrayList<Item> Zone){
	    // Item(Name, Description)
	    Zone.add(new Item("Gold Coin",
	        "A small disc made of lustrous metal"));
	    Zone.add(new Item("Iron Key",
	        "A heavy iron key with a simple cut"));
	}
	 
	private void buildWeapons(ArrayList<Weapon> Zone){
	    // Weapon(Name, Description, Damage, Hit Rate)
	    Zone.add(new Weapon("Iron Dagger",
	        "A short blade made of iron with a leather-bound hilt",
	        5, 10.0));
	    Zone.add(new Weapon("Excalibur",
	        "The legendary blade, bestowed upon you by the Lady of the Lake",
	        35, 35.0));
	}
	 
	private void buildArmor(ArrayList<Armor> Zone){
	    // Armour(Name, Description, Defense, Slot)
	    Zone.add(new Armor("Leather Cuirass",
	        "Torso armour made of tanned hide", 4, Armour::Slot::TORSO));
	}
	 
	private void buildZone(ArrayList<Zone> Database,
				   ArrayList<Item> items,
				   ArrayList<Weapon> weapons,
				   ArrayList<Armor> armor,
				   ArrayList<Creature> creatures){
	    // Area definitions are somewhat more complicated:
	    Database.add(new Area(new Dialogue(        // Standard dialogue definiton
	        "You are in room 1",              // Description
	        {"Go to room 2", "Search"}),      // Choices
	        Inventory(                        // Area inventory
	        {
	            std::make_pair(&items[0], 5)  // Pair of item and quantity
	 
	        },
	        {
	            std::make_pair(&weapons[0], 1)// Pair of weapon and quantity
	        },
	        {
	            std::make_pair(&armour[0], 1) // Pair of armour and quantity
	        }),
	        {                                 // Creatures
	        }));
	 
	    atlas.push_back(Area(Dialogue(
	        "You are in room 2",
	        {"Go to room 1", "Search"}),
	        Inventory(
	        {
	            std::make_pair(&items[0], 10),
	            std::make_pair(&items[1], 1)
	        },
	        {
	        },
	        {
	        }),
	        {
	            &creatures[0]
	        }));
	 
	    return;
	}
}