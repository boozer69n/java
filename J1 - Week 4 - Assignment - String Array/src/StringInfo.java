//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 4 - Assignment - Strings, Arrays, Classes 
//#################################################################################################
//Create a program that prompts the user to enter two Strings. Then display the Strings in
//alphabetical order. If the two Strings are equal, display a message that they are equal rather
//than printing them in alphabetical order.
//#################################################################################################

import java.util.Arrays;

public class StringInfo{	
	private String stringPhrase; //saving string
	private char[] arraySort; //saving sorted version of the phrase
	private char[] arrayString; //saving string into array
	
	public void setString(String phrase){ //setting the string
		this.stringPhrase = phrase; //setting input to private variable
	} //end method
	
	public String getString(){ //returning the string
		return this.stringPhrase; //returning string
	} //end method
		
	public void setArray(){ //saving info to array
		if( this.stringPhrase != null && this.stringPhrase.length() > 0){ // double checking for input
			this.arrayString = new char[this.stringPhrase.length()]; //creating array from string
	        for(int i = 0; i < this.stringPhrase.length(); i++){ //loop to input to array
	            this.arrayString[i] = this.stringPhrase.charAt(i); //inputting to array one char at a time
	        } //end loop
	    } //end if
	} //end method
	
	public void setArraySort(){ //saving to a new array to sort
		int length = this.arrayString.length; // saving length of array
		this.arraySort = new char[length]; //creating new array
		for (int i = 0; i < length; i++){ //loop to go through array
			this.arraySort[i] = this.arrayString[i]; //saving one char at a time into new array
		} //end loop
		Arrays.sort(this.arraySort); //sorting new array
	} //end method
	
	public char[] getArraySort(){ //return new array sorted
		return this.arraySort; //returns sorted array
	} //end method
	
	public void display(){ //method for displaying everything
		System.out.println("Original String: " + this.stringPhrase); //displays original input
		System.out.println("Original Array: " + Arrays.toString(this.arrayString)); //displays original info in array form
		System.out.print("Sorted String: "); //displays input sorted
		for (int loop = 0; loop < (this.arraySort.length); loop++){ //loop going through sorted array
			if (this.arraySort[loop] != ' '){ //if checking for blanks
				System.out.print(this.arraySort[loop]); //displaying all but blanks
			} //end if
		} //end loop
		System.out.println(""); //to go to next line
		System.out.println("Sorted Array: " + Arrays.toString(this.arraySort)); //displaying sorted array in array form
	} //end method
}