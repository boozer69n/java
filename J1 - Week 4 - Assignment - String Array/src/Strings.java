//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 4 - Assignment - Strings, Arrays, Classes 
//#################################################################################################
//Create a program that prompts the user to enter two Strings. Then display the Strings in
//alphabetical order. If the two Strings are equal, display a message that they are equal rather
//than printing them in alphabetical order.
//#################################################################################################

import java.util.Scanner;

public class Strings { //begin class
	public static void main(String[] args){ //begin main
	
		StringInfo stringOne = new StringInfo(); //creating string 1 object
		StringInfo stringTwo = new StringInfo(); //creating string 2 object
		
		Scanner i = new Scanner(System.in); //creating scanner object
		
		setString(stringOne, i, "One"); //running method to get string from user
		setString(stringTwo, i, "Two"); //running method to get string from user
		
		stringOne.setArray(); //setting the string as an array
		stringTwo.setArray(); //setting the string as an array
		
		stringOne.setArraySort(); //creating a separate sorted array
		stringTwo.setArraySort(); //creating a separate sorted array
		
		if (stringOne.getString().equals(stringTwo.getString())){ //check to see if they are the same
			System.out.println(""); //for display
			System.out.println("Both Strings/Phrases are the same."); //displaying message that they are the same
		}else{ //begin else
			//displaying info for string one
			System.out.println("");
			System.out.println("String One");
			System.out.println("----------");
			stringOne.display();
			
			//displaying info for string two
			System.out.println("");
			System.out.println("String Two");
			System.out.println("----------");
			stringTwo.display();	
		} //end else
		i.close(); //closing scanner
	} //end main
	
	static void setString(StringInfo stringNumber, Scanner i, String num){ //method to set strings
		boolean shouldExit = false; //setting exit variable
		while (!shouldExit){ //loop to check for input
			String word = null; //creating string to later save input
			System.out.println(""); //space for cleaner ui	
			System.out.print("String/Phrase " + num + ": "); //requesting user for string/phrase
			word = i.nextLine(); //saving to variable
			if ( word != null && word.length() > 0){ //checking for actual input
				stringNumber.setString(word); //sending to object
				shouldExit = true; //exit loop
			}else{ //else instructions for user
				System.out.println("An Error has occured, you are trying to crash my program");
				System.out.println("Please follow the below steps:");
				System.out.println("1 - Place hand behind head.");
				System.out.println("2 - Push forward and downward.");
				System.out.println("3 - After your face smacks the keyboard please hit enter.");
				System.out.println("Thank You for Shopping at Walmart =)");
			} //end else
		} //end loop
	} //end method
} //end class