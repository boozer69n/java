//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 2 - Project - Static Variables and Methods
//#################################################################################################
//Create a SavingsAccount class. This class should contain a static variable that stores the annual
//interest rate for all account holders. Each object of the class should have a private instance
//variable that stores the savings balance (indicates the amount of money that is in the account).
//Create a method that will calculate the amount of interest that will be accumulated each month
//multiplying the savings balance by the annual interest rate / 12. Add the amount of interest to
//the savings balance. Create a static method that will allow you to change the annual interest
//rate value.

//Please write a program to test your SavingsAccount class. You will need to instantiate two
//savings account objects. Balance 1 should be $2000, and Balance 2 should be $3000. Set the
//annual interest rate to 4% and calculate the monthly interest for the next 12 months and print
//the final balances for both accounts. Then, change the annual interest rate to 5% and calculate
//the monthly interest for the next 12 months. Print the final balances for both accounts.
//#################################################################################################
import java.text.DecimalFormat; //included to format numbers for clarity

public class SavingsAccount {
	private double originalBalance; //original balance of account
	private double savingsBalance; //total balance of account
	private double monthlyGain; //monthly interest gain
	private double yearlyGain; //yearly interest gain
	public static double interestRate; //interest rate
	
	public double getOriginalBalance() { //returns the original balance
		return originalBalance;
	}
	public void setOriginalBalance(double originalBalance) { //sets original balance
		this.originalBalance = originalBalance;
		update();
	}
	public double getSavingsBalance() { //returns total balance
		return savingsBalance;
	}
	public double getInterestRate() { //returns interest rate
		return interestRate;
	}
	public void update(){ //updates account information
		this.yearlyGain = ( this.originalBalance * (interestRate / 100) );
		this.monthlyGain = (this.yearlyGain / 12);
		this.savingsBalance = ( this.yearlyGain + this.originalBalance);
	}
	public void display() { //displays info on account
		System.out.println( "Monthly Gain: $" + decimalPlaces(this.monthlyGain) );		
		System.out.println( "Total Gain: $" + decimalPlaces(this.yearlyGain) );
		System.out.println( "Original Balance: $" + decimalPlaces(this.originalBalance) );
		System.out.println( "New Balance: $" + decimalPlaces(this.savingsBalance) );
		System.out.println( "Interest Rate: " + decimalPlaces(interestRate) + "%" );
	}
	public String decimalPlaces(double number){ //formats the numbers to 2 decimal places
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(number);
	}
}