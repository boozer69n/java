//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 2 - Project - Static Variables and Methods
//#################################################################################################
//Create a SavingsAccount class. This class should contain a static variable that stores the annual
//interest rate for all account holders. Each object of the class should have a private instance
//variable that stores the savings balance (indicates the amount of money that is in the account).
//Create a method that will calculate the amount of interest that will be accumulated each month
//multiplying the savings balance by the annual interest rate / 12. Add the amount of interest to
//the savings balance. Create a static method that will allow you to change the annual interest
//rate value.

//Please write a program to test your SavingsAccount class. You will need to instantiate two
//savings account objects. Balance 1 should be $2000, and Balance 2 should be $3000. Set the
//annual interest rate to 4% and calculate the monthly interest for the next 12 months and print
//the final balances for both accounts. Then, change the annual interest rate to 5% and calculate
//the monthly interest for the next 12 months. Print the final balances for both accounts.
//#################################################################################################

import java.util.Scanner;

public class BalanceCheck { //begin class
	public static void main(String[] args) { //begin main
		
		SavingsAccount accountOne = new SavingsAccount(); //creating accountOne object
		SavingsAccount accountTwo = new SavingsAccount(); //creating accountTwo object
		
		accountOne.setOriginalBalance(2000); //sets default
		accountTwo.setOriginalBalance(3000); //sets default
		SavingsAccount.interestRate = 4; //sets default
		
		Scanner i = new Scanner(System.in); //creating scanner object		

		boolean shouldExit = false; //setting exit variable
		while (!shouldExit) //loop until user is done
		{ //begin while loop
			
			int userSelection = 0; //variable saving users selection
			//ui for selection
			System.out.println(" ");
			System.out.println("#########################");
			System.out.println("#Please Make a Selection#");
			System.out.println("#########################");
			System.out.println("1 - Update Account One");
			System.out.println("2 - Update Account Two");
			System.out.println("3 - Update Interest Rate");
			System.out.println("4 - Display Info");
			System.out.println("5 - Display Assignment for Professor");
			System.out.println("    WARNING - RESETS VALUES!!");
			System.out.println("6 - Exit");
			System.out.println("#########################");
			System.out.print("Selection: ");
			
			if (i.hasNextInt()) //verifying input is an integer
			{ //begin true
				userSelection = i.nextInt(); //if its an integer save
			} //end true
			else
			{ //begin else
				System.out.println("Invalid Input"); //prompt if invalid
			} //end else
						
			switch (userSelection) //using the input to determine course
			{ //begin switch
			case 1: //selection 1
				setAccount(accountOne); //runs through method to set account one
				break; //end selection 1
			
			case 2: //selection 2
				setAccount(accountTwo); //runs through method to set account two
				break; //end selection 2
				
			case 3: //selection 3
				setRating(); //runs method to set interest rate
				break; //end selection 3
				
			case 4: //selection 4
				accountOne.update(); //updates the information within the account
				accountTwo.update(); //updates the information within the account
				System.out.println(" "); //for display
				System.out.println("-----------------------"); //for display
				System.out.println("Account One Information"); //for display
				System.out.println("-----------------------"); //for display
				accountOne.display(); //displays all the account information
				System.out.println(" "); //for display
				System.out.println("-----------------------"); //for display
				System.out.println("Account Two Information"); //for display
				System.out.println("-----------------------"); //for display
				accountTwo.display(); //displays all the account information
				break; //end selection 4
				
			case 5: //selection 5
				SavingsAccount.interestRate = 4; //setting interest rate to 4%
				accountOne.setOriginalBalance(2000); //setting account one balance to 2000
				accountTwo.setOriginalBalance(3000); //setting account one balance to 3000
				accountOne.update(); //updates the information within the account
				accountTwo.update(); //updates the information within the account
				System.out.println(" "); //for display
				System.out.println("-----------------------"); //for display
				System.out.println("Account One Information"); //for display
				System.out.println("-----------------------"); //for display
				accountOne.display(); //displays all the account information
				System.out.println(" "); //for display
				System.out.println("-----------------------"); //for display
				System.out.println("Account Two Information"); //for display
				System.out.println("-----------------------"); //for display
				accountTwo.display(); //displays all the account information
				
				SavingsAccount.interestRate = 5; //setting interest rate to 5%
				accountOne.setOriginalBalance(2000); //setting account one balance to 2000
				accountTwo.setOriginalBalance(3000); //setting account one balance to 3000
				accountOne.update(); //updates the information within the account
				accountTwo.update(); //updates the information within the account
				System.out.println("Account One Information"); //for display
				System.out.println("-----------------------"); //for display
				accountOne.display(); //displays all the account information
				System.out.println("Account Two Information"); //for display
				System.out.println("-----------------------"); //for display
				accountTwo.display(); //displays all the account information
				break; //ending selection 5
				
			case 6: //selection 6
				System.out.println("Bye o/"); //saying bye
				shouldExit = true; //exiting while loop
				break; //ending selection 6
				
			default: //default if 1-6 is not selected
				System.out.println("Invalid Input"); //prompt if invalid
				break; //ending default
				
			} //end switch
		} //end while loop
		i.close(); //closing scanner
	} //end main
	
	static void setRating() //method for setting interest rate
	{
		boolean shouldExit = false; //exit variable
		while (!shouldExit) //while loop
		{ //begin loop
			System.out.println(" "); //for display
			System.out.print("New Interest Rate: "); //prompt for new interest rate
			Scanner tempRate = new Scanner(System.in); //saving to temp variable
			if (tempRate.hasNextDouble()) //verifying input is an double
			{ //begin true
				//if valid input saves it to interestRate
				SavingsAccount.interestRate = tempRate.nextDouble();
				shouldExit = true; //sets exit loop variable
			} //end true
			else if (tempRate.hasNextInt()) //verifying input is an int
			{ //begin true
				//if valid input saves it to interestRate
				SavingsAccount.interestRate = tempRate.nextInt();
				shouldExit = true; //sets exit loop variable
			} //end true
			else
			{ //begin else
				System.out.println("Invalid Input"); //prompt if invalid
			} //end else
		} //end loop		
	} //end method
	
	static void setAccount(SavingsAccount account) //method for setting account balance
	{
		boolean shouldExit = false;
		while (!shouldExit)
		{
			System.out.println(" "); //for display
			System.out.println("Account One"); //for display
			System.out.print("Original Balance: "); //requesting user to input balance
			Scanner accTemp = new Scanner(System.in); //saving to temp variable
			if (accTemp.hasNextDouble()) //verifying input is an double
			{ //begin true
				//if valid input saves it to account
				account.setOriginalBalance(accTemp.nextDouble());
				shouldExit = true; //sets exit loop variable
			} //end true
			else if (accTemp.hasNextInt()) //verifying input is an int
			{ //begin else
				//if valid input saves it to account
				account.setOriginalBalance(accTemp.nextInt());
				shouldExit = true; //sets exit loop variable
			} //end else
			else
			{ //begin else
				System.out.println("Invalid Input"); //prompt if invalid
			} //end else
		} //end loop
	} //end method
} //end class
