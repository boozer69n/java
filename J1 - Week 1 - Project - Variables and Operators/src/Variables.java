//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 1 - Project - Variables and Operators
//#################################################################################################
//The purpose of this project is to familiarize you with the basic concepts of:
//	Declaring and using variables
//	Console input and output
//	Basic mathematical and relational operators
//
//You must write a Java program that allows the user to input three integer values. Your program
//will then determine and display the sum (add them up), average (add them up and divide by three),
//and product (multiply them together). The output of your program should be clear and easy to read
//
//For this project you must:
//	Declare and use at least 3 integer variables.
//	Declare and use at least 1 float variable.
//	Use the +, *, and / math operators.
//#################################################################################################

import java.util.Scanner; //importing the ability to scan for input

public class Variables { //begin class variables
	public static void main(String variables[]) //begin main
	{	
		int numOne = 0; //defining integer
		int numTwo = 0; //defining integer
		int numThree = 0; //defining integer
		
		//title display
		System.out.println("------------------------------------------");
		System.out.println("Week 1 - Project - Variables and Operators");
		System.out.println("------------------------------------------");
		
		int exitOne = 1; //setting exit variable
		while (exitOne != 0) //loop to make sure input is valid
		{
			System.out.print("Please enter the first Integer: "); //prompting the user for input
			Scanner i = new Scanner(System.in); //saving the input to a temp variable
			if (i.hasNextInt()) //verifying input is an integer
			{
				numOne = i.nextInt(); //if its an integer save
				exitOne = 0; //variable to exit loop
			}
			else
			{
				System.out.println("Invalid Input"); //if input is not valid
			}
			i.close();
		}
		
		int exitTwo = 1; //setting exit variable
		while (exitTwo != 0) //loop to make sure input is valid
		{
			System.out.print("Please enter the second Integer: "); //prompting the user for input
			Scanner i = new Scanner(System.in); //saving the input to a temp variable
			if (i.hasNextInt()) //verifying input is an integer
			{
				numTwo = i.nextInt(); //if its an integer save
				exitTwo = 0; //variable to exit loop
			}
			else
			{
				System.out.println("Invalid Input"); //if input is not valid
			}
			i.close();
		}
		
		int exitThree = 1; //setting exit variable
		while (exitThree != 0) //loop to make sure input is valid
		{
			System.out.print("Please enter the third Integer: "); //prompting the user for input
			Scanner i = new Scanner(System.in); //saving the input to a temp variable
			if (i.hasNextInt()) //verifying input is an integer
			{
				numThree = i.nextInt(); //if its an integer save
				exitThree = 0; //variable to exit loop
			}
			else
			{
				System.out.println("Invalid Input"); //if input is not valid
			}
			i.close();
		}
		
		//title display
		System.out.println("------------------------------------------");
		System.out.println("------------Operator Selection------------");
		System.out.println("------------------------------------------");
		
		int exit = 0; //setting exit variable
		while (exit != 5) //loop until user is done
		{
			//UI for options
			System.out.println(" ");
			System.out.println("Your numbers: " + numOne + ", " + numTwo + ", " + numThree);
			System.out.println("Please choose operation");
			System.out.println("1 - Addition (+)");
			System.out.println("2 - Subtraction (-)");
			System.out.println("3 - Multiplication (*)");
			System.out.println("4 - Average (X)");
			System.out.println("5 - Exit  o/");
			System.out.println("-----------------------");
			System.out.print("Selection: ");
			
			int userSelection = 0; //defining user input variable
			Scanner i = new Scanner(System.in); //saving the input to a temp variable
			if (i.hasNextInt()) //verifying input is an integer
			{
				userSelection = i.nextInt(); //if its an integer save
			}
			else
			{
				System.out.println("Invalid Input"); //prompt if invalid
			}
			i.close();

			switch (userSelection) //listing options
			{
			case 1: //selection 1
				System.out.println("Addition (+)"); //displaying selection
				System.out.println("------------"); //line for prettiness
				int add = numOne + numTwo + numThree; //saving answer
				//displaying answer
				System.out.println(numOne + " + " + numTwo + " + " + numThree + " = " + add);
				break; //ending switch
				
			case 2: //selection 2
				System.out.println("Subtraction (-)"); //displaying selection
				System.out.println("---------------"); //line for prettiness
				int sub = numOne - numTwo - numThree; //saving answer
				//displaying answer
				System.out.println(numOne + " - " + numTwo + " - " + numThree + " = " + sub);
				break;
				
			case 3: //selection 3
				System.out.println("Multiplication (*)"); //displaying selection
				System.out.println("------------------"); //line for prettiness
				int multi = numOne * numTwo * numThree; //saving answer
				//displaying answer
				System.out.println(numOne + " * " + numTwo + " * " + numThree + " = " + multi);
				break;
				
			case 4: //selection 4
				System.out.println("Average (X)"); //displaying selection
				System.out.println("------------------"); //line for prettiness
				float avg = ((numOne + numTwo + numThree)/3); //saving answer
				//displaying answer
				System.out.println(numOne + " + " + numTwo + " + " + numThree + " / 3 = " + avg);
				break;
				
			case 5: //selection 5
				System.out.println("Bye bye now  o/"); //displaying selection
				exit = 5; //exiting program
				break;
				
			default:
				System.out.println("Invalid Input"); //prompt if invalid
				break;
				
			}
			
		}
		
	}

}