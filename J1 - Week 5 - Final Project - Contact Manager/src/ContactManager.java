/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Final Project - Contact Manager
###################################################################################################
The purpose of this project is to familiarize you with the details of leveraging polymorphic
behavior in Java.

You must write a Java program that acts as a basic contact manager. The application will begin
with a simple menu that allows the user to choose to 1) Add a new contact, 2) Look up a contact
by last name, or3) Exit the application.

When the user chooses to add a contact, the application will ask the user if they want to add a
regular contact or a friend. If the user wants to add a new regular contact then the application
will ask for the first name, last name, phone number, and address info (street, city, state, and
zip) for that person. If the user wants to add a new friend then the application will ask the
user for the first name, last name, phone number, address info, as well as an email address,
and birth date.

When the user chooses to lookup a contact by last name, the application will ask the user for
the last name, and then the application will display the full information (first name, last
name, phone number, address info, and email and birthdate where applicable) for all contacts
with that last name.

For this project you must:

    Create an Address class with the following members:
        Getter and setter for street
        Getter and setter for city
        Getter and setter for state
        Getter and setter for zip
    Create a Contact base class with the following members:
        Address
        Getter and setter for first name
        Getter and setter for last name
        Getter and setter for phone number
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, and address info for this contact
    Create a Friend derived class that extends the Contact base class, with the following members:
        Getter and setter for email address
        Getter and setter for birth date
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, address info, and email address, and birth date for this contact

#################################################################################################*/

import java.util.ArrayList; //importing array lists
import java.util.Scanner; //importing scanner

public class ContactManager{ //begin class
	static Scanner i = new Scanner(System.in); //scanner object
	static ArrayList< Contact > contacts = new ArrayList< Contact >(); //array list for regular contacts
	
	public static void main( String[] args ){ //begin main
		boolean shouldExit = false; //exit variable
		while(!shouldExit){ //begin loop
			System.out.println(""); //for display
			//ui for selection
			System.out.println("Contact Manager");
			System.out.println("---------------");
			System.out.println("1 - Add a new contact");
			System.out.println("2 - Look up a contact");
			System.out.println("3 - Exit");
			System.out.print("Selection: ");
			if(i.hasNextInt()){ //checking for int
				int userSelection = i.nextInt(); //saving int
				i.nextLine(); //removing return
				switch(userSelection){ //running selection
					case 1: //selection 1
						boolean caseOneExit = false; //exit variable
						while (!caseOneExit){ //begin loop
							System.out.println(""); //for display
							//ui for contact type
							System.out.println("Type of contact");
							System.out.println("---------------");
							System.out.println("1 - Friend contact");
							System.out.println("2 - Regular contact");
							System.out.print("Selection: ");
							if(i.hasNextInt()){ //checking for int
								int caseOneSelection = i.nextInt(); //saving int
								i.nextLine(); //removing return
								switch(caseOneSelection){ //running selection
									case 1: //selection 1
										createContact("friend"); //creating a contact method
										caseOneExit = true; //exit variable
										break; //end selection
									case 2: //selection 2
										createContact("regular"); //creating contact method
										caseOneExit = true; //exit variable
										break; //end selection
									default: //default selection
										System.out.println("Invalid Input"); //prompt for invalid input
										break; //end switch
								} //end switch
							}else{ //begin else
								i.nextLine(); //to remove the invalid input from scanner
								System.out.println("Invalid Input"); //prompt for invalid input
							} //end else
						} //end loop
						break; //end selection 1
					case 2: //selection 2
						System.out.println(""); //for display
						System.out.println("Please enter the last name of the person you are searching for");
						System.out.print("Name: "); //requesting last name
						String lName = i.nextLine(); //saving input
						lookup(lName); //method to look up contact and display
						break; //end selection 2
					case 3: //selection 3
						shouldExit = true; //exit variable
						System.out.println(""); //for display
						System.out.println("Bye bye o/"); //waving bye
						break; //end selection 3
					default: //default selection
						System.out.println(""); //for display
						System.out.println("Invalid Input"); //prompt for invalid input
						break; //end default
				} //end switch
			}else{ //begin else
				i.nextLine(); //to remove the invalid input from scanner
				System.out.println(""); //for display
				System.out.println("Invalid Input"); //prompt for invalid input
			} //end else
		} //end loop
	} //end main
	
	public static void createContact(String type){ //creating contact
		System.out.println(""); //for display
		//requesting and saving information on the contact
		System.out.println("Information");
		System.out.println("-----------");
		System.out.print("First Name: ");
		String fName = i.nextLine();
		System.out.print("Last Name: ");
		String lName = i.nextLine();
		System.out.print("State: ");
		String state = i.nextLine();
		System.out.print("Zip Code: ");
		String zip = i.nextLine();
		System.out.print("City: ");
		String city = i.nextLine();
		System.out.print("Street Address: ");
		String street = i.nextLine();
		System.out.print("Phone Number: ");
		String number = i.nextLine();
		if(type.equalsIgnoreCase("regular")){ //begin if for regular contact
			Contact c = new Contact(fName, lName, number, street, city, state, zip); //create contact
			contacts.add(c); //adds contact to array
			System.out.println("Contact added"); //display confirmation of addition
		}else if(type.equalsIgnoreCase("friend")){ //begin for friend contact
			//requesting additional info for friend contact
			System.out.print("Email: ");
			String email = i.nextLine();
			System.out.print("Birthday: ");
			String bday = i.nextLine();
			Friend f = new Friend(fName, lName, number, street, city, state, zip, email, bday); //creating friend contact
			contacts.add(f); //adding contact to array
			System.out.println("Friend added"); //display confirmation of addition
		}else{ //begin else
			System.out.print("Invalid Type"); //display error
		} //end else
	} //end method
	
	public static void lookup(String lName){ //looking up and displaying contacts
		System.out.println("");
		System.out.println("     Contacts     ");
		System.out.println("##################");
		for (int j = 0; j < contacts.size(); j++){ //begin loop
			if(lName.equalsIgnoreCase(contacts.get(j).getLastName())){ //searching for match
				contacts.get(j).getAllInfoAsString(); //displaying matches
			} //end if
		} //end loop
		System.out.println("");
		System.out.println("##################");
	} //end method

} //end class