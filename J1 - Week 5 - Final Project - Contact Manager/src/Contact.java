/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Final Project - Contact Manager
###################################################################################################
The purpose of this project is to familiarize you with the details of leveraging polymorphic
behavior in Java.

You must write a Java program that acts as a basic contact manager. The application will begin
with a simple menu that allows the user to choose to 1) Add a new contact, 2) Look up a contact
by last name, or3) Exit the application.

When the user chooses to add a contact, the application will ask the user if they want to add a
regular contact or a friend. If the user wants to add a new regular contact then the application
will ask for the first name, last name, phone number, and address info (street, city, state, and
zip) for that person. If the user wants to add a new friend then the application will ask the
user for the first name, last name, phone number, address info, as well as an email address,
and birth date.

When the user chooses to lookup a contact by last name, the application will ask the user for
the last name, and then the application will display the full information (first name, last
name, phone number, address info, and email and birthdate where applicable) for all contacts
with that last name.

For this project you must:

    Create an Address class with the following members:
        Getter and setter for street
        Getter and setter for city
        Getter and setter for state
        Getter and setter for zip
    Create a Contact base class with the following members:
        Address
        Getter and setter for first name
        Getter and setter for last name
        Getter and setter for phone number
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, and address info for this contact
    Create a Friend derived class that extends the Contact base class, with the following members:
        Getter and setter for email address
        Getter and setter for birth date
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, address info, and email address, and birth date for this contact

#################################################################################################*/

public class Contact{ //begin class
	private String firstName; //stores first name
	private String lastName; //stores last name
	private String phoneNumber; //stores phone number
	private Address a; //stores address info
	
	//constructor
	public Contact(String firstName,
				   String lastName,
				   String phoneNumber,
				   String street,
				   String city,
				   String state,
				   String zip){
		this.firstName = firstName; //saves first name
		this.lastName = lastName; //saves last name
		this.phoneNumber = phoneNumber; //saves phone number
		this.a = new Address(street, city, state, zip); //fills in address info
	} //end constructor
	
	public void setFirstName(String firstName){ //setting first name method
		this.firstName = firstName; //saving first name
	} //end method
	
	public String getFirstName(){ //returning first name method
		return this.firstName; //returns first name
	} //end method
	
	public void setLastName(String lastName){ //setting last name
		this.lastName = lastName; //saving last name
	} //end method
	
	public String getLastName(){ //returning last name method
		return this.lastName; //returns last name
	} //end method
	
	public void setPhoneNumber(String phoneNumber){ //setting phone number method
		this.phoneNumber = phoneNumber; //saving phone number
	} //end method
	
	public String getPhoneNumber(){ //returning phone number method
		return this.phoneNumber; //returns phone number
	} //end method

	public void getAllInfoAsString(){ //displaying information method
		//displaying contact information
		System.out.printf("\nName: %s %s\nNumber: %s", this.firstName, this.lastName, this.phoneNumber);
		this.a.getAllInfoAsString(); //displaying address info
	} //end method

} //end class