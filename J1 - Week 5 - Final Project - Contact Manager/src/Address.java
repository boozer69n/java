/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Final Project - Contact Manager
###################################################################################################
The purpose of this project is to familiarize you with the details of leveraging polymorphic
behavior in Java.

You must write a Java program that acts as a basic contact manager. The application will begin
with a simple menu that allows the user to choose to 1) Add a new contact, 2) Look up a contact
by last name, or3) Exit the application.

When the user chooses to add a contact, the application will ask the user if they want to add a
regular contact or a friend. If the user wants to add a new regular contact then the application
will ask for the first name, last name, phone number, and address info (street, city, state, and
zip) for that person. If the user wants to add a new friend then the application will ask the
user for the first name, last name, phone number, address info, as well as an email address,
and birth date.

When the user chooses to lookup a contact by last name, the application will ask the user for
the last name, and then the application will display the full information (first name, last
name, phone number, address info, and email and birthdate where applicable) for all contacts
with that last name.

For this project you must:

    Create an Address class with the following members:
        Getter and setter for street
        Getter and setter for city
        Getter and setter for state
        Getter and setter for zip
    Create a Contact base class with the following members:
        Address
        Getter and setter for first name
        Getter and setter for last name
        Getter and setter for phone number
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, and address info for this contact
    Create a Friend derived class that extends the Contact base class, with the following members:
        Getter and setter for email address
        Getter and setter for birth date
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, address info, and email address, and birth date for this contact

#################################################################################################*/

public class Address { //begin class
	private String street; //storing street info
	private String city; //storing city info
	private String state; //storing state info
	private String zip; //storing zip code
	
	public Address(String street, String city, String state, String zip){ //begin constructor
		this.street = street; //saving street
		this.city = city; //saving city
		this.state = state; //saving state
		this.zip = zip; //saving zip code
	} //end constructor
	
	public void setStreet(String street){ //setting street method
		this.street = street; //saving street
	} //end method
	
	public String getStreet(){ //return street method
		return this.street; //returns street
	} //end method
	
	public void setCity(String city){ //setting city method
		this.city = city; //saving city
	} //end method
	
	public String getCity(){ //returning city method
		return this.city; //returns city
	} //end method
	
	public void setState(String state){ //setting state method
		this.state = state; //saving state
	} //end method
	
	public String getState(){ //returning state method
		return this.state; //returns state
	} //end method
	
	public void setZip(String zip){ //setting zip code method
		this.zip = zip; //saving zip code
	} //end method
	
	public String getZip(){ //returning zip code method
		return this.zip; //returns zip code
	} //end method
	
	public void getAllInfoAsString(){ //displaying info method
		//displays address info
		System.out.printf("\nAddress: %s\n         %s, %s %s", this.street, this.city, this.state, this.zip);
	} //end method
} //end class