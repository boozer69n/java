/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 5 - Final Project - Contact Manager
###################################################################################################
The purpose of this project is to familiarize you with the details of leveraging polymorphic
behavior in Java.

You must write a Java program that acts as a basic contact manager. The application will begin
with a simple menu that allows the user to choose to 1) Add a new contact, 2) Look up a contact
by last name, or3) Exit the application.

When the user chooses to add a contact, the application will ask the user if they want to add a
regular contact or a friend. If the user wants to add a new regular contact then the application
will ask for the first name, last name, phone number, and address info (street, city, state, and
zip) for that person. If the user wants to add a new friend then the application will ask the
user for the first name, last name, phone number, address info, as well as an email address,
and birth date.

When the user chooses to lookup a contact by last name, the application will ask the user for
the last name, and then the application will display the full information (first name, last
name, phone number, address info, and email and birthdate where applicable) for all contacts
with that last name.

For this project you must:

    Create an Address class with the following members:
        Getter and setter for street
        Getter and setter for city
        Getter and setter for state
        Getter and setter for zip
    Create a Contact base class with the following members:
        Address
        Getter and setter for first name
        Getter and setter for last name
        Getter and setter for phone number
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, and address info for this contact
    Create a Friend derived class that extends the Contact base class, with the following members:
        Getter and setter for email address
        Getter and setter for birth date
        GetAllInfoAsString method that returns a formatted string with the first name, last name,
        phone number, address info, and email address, and birth date for this contact

#################################################################################################*/

public class Friend extends Contact{ //begin class
	private String email; //email info
	private String bday; //birthday info
	
	//constructor
	public Friend(String firstName,
				  String lastName,
				  String phoneNumber,
				  String street,
				  String city,
				  String state,
				  String zip,
				  String email,
				  String bday){
		super(firstName, lastName, phoneNumber, street, city, state, zip); //sending to parent class
		this.email = email; //saving email
		this.bday = bday; //saving birthday
	} //end constructor
	
	public void setEmail(String email){ //setting email method
		this.email = email; //saves email
	} //end method
	
	public String getEmail(){ //returning email method
		return this.email; //returns email
	} //end method
	
	public void setBday(String bday){ //setting birthday method
		this.bday = bday; //saving birthday
	} //end method
	
	public String getBday(){ //returning birthday
		return this.bday; //returns birthday
	} //end method
	
	public void getAllInfoAsString(){ //display method
		super.getAllInfoAsString(); //running parent display method
		System.out.printf("\nEmail: %s\nBirthday: %s\n", this.email, this.bday); //displaying friend data
	} //end method

} //end class