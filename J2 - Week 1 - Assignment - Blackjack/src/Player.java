import java.util.ArrayList;

public class Player {
	private String name;
	//private Card[] hand = new Card[10];
	private ArrayList< Card > hand = new ArrayList< Card >();
	
	//constructor setting name of player
	public Player(String myName){
		this.name = myName;
		this.emptyHand();
	}
	
	//empties players hand and resetting number of cards
	public void emptyHand(){
		this.hand.clear();
	} //emptyHand
	
	//adding a card to the hand and returning if a bust happens
	public boolean addCard(Card oneCard){
		this.hand.add(oneCard);
		return (this.getSum(true) <= 21);
	} //addCard
	
	//returning the value of your hand
	public int getSum(boolean dealer){
		int handSum = 0;
		int cardNum = 0;
		int numAce = 0;
		
		//looping through hand and assigning values
		for (int i = 0; i < this.hand.size(); i++){
			if (dealer == false){i++;} //skip first card if dealer
			cardNum = this.hand.get(i).getMyNumber();
			
			if (cardNum == 1){ //if ace
				numAce++;
				handSum +=11;
			}else if (cardNum > 10){ //if face
				handSum += 10;
			}else{ //if number
				handSum += cardNum;
			}
		} //for

		//checking if the ace needs to be 1 or 11
		while (handSum > 21 && numAce > 0){
			handSum -=10;
			numAce--;
		}		
		return handSum;
	} //getSum
	
	//returning the name of the player
	public String getName(){
		return this.name;
	}
	
	//printing the card, dealerCard is for the dealer
	public void printHand(boolean dealerCard){
		int dealerBlank;
		System.out.printf("\n\n%s's Cards : %d", this.name, this.getSum(dealerCard));
		
		for (int j = 1; j <= 6; j++){
			System.out.println("");
			for (int i = 0; i < this.hand.size(); i++){
				if (dealerCard == false && i == 0){
					dealerBlank = 4;
				}else{
					dealerBlank = this.hand.get(i).getMySuit();
				}
				switch(dealerBlank){
				case 0:
					printClubs(this.hand.get(i).toString(), j);
					break;
				case 1:
					printDiamonds(this.hand.get(i).toString(), j);
					break;
				case 2:
					printHearts(this.hand.get(i).toString(), j);
					break;
				case 3:
					printSpades(this.hand.get(i).toString(), j);
					break;
				case 4:
					printBack(j);
					break;
				} //switch
			} //for
		} //for
	} //printHand

	//displaying ansii heart card
	private void printHearts(String value, int line) {
		switch (line){
		case 1:
			System.out.print(".--------. ");
			break;
		case 2:
			System.out.printf("|%2s_  _  | ", value);
			break;
		case 3:
			System.out.print("| ( \\/ ) | ");
			break;
		case 4:
			System.out.print("|  \\  /  | ");
			break;
		case 5:
			System.out.printf("|   \\/ %2s| ", value);
			break;
		case 6:
			System.out.print("'--------' ");
			break;
		} //switch
	} //printHearts

	//displaying ansii diamonds card
	private void printDiamonds(String value, int line) {
		switch (line){
		case 1:
			System.out.print(".--------. ");
			break;
		case 2:
			System.out.printf("|%2s /\\   | ", value);
			break;
		case 3:
			System.out.print("|  /  \\  | ");
			break;
		case 4:
			System.out.print("|  \\  /  | ");
			break;
		case 5:
			System.out.printf("|   \\/ %2s| ", value);
			break;
		case 6:
			System.out.print("'--------' ");
			break;
		} //switch
	} //printDiamonds

	//displaying ansii clubs card
	private void printClubs(String value, int line) {
		switch (line){
		case 1:
			System.out.print(".--------. ");
			break;
		case 2:
			System.out.printf("|%2s _    | ", value);
			break;
		case 3:
			System.out.print("|  ( )   | ");
			break;
		case 4:
			System.out.print("| (_^_)  | ");
			break;
		case 5:
			System.out.printf("|   I  %2s| ", value);
			break;
		case 6:
			System.out.print("'--------' ");
			break;
		} //switch
	} //printClubs

	//displaying ansii spades card
	private void printSpades(String value, int line) {
		switch (line){
		case 1:
			System.out.print(".--------. ");
			break;
		case 2:
			System.out.printf("|%2s .    | ", value);
			break;
		case 3:
			System.out.print("|  / \\   | ");
			break;
		case 4:
			System.out.print("| (_,_)  | ");
			break;
		case 5:
			System.out.printf("|   I  %2s| ", value);
			break;
		case 6:
			System.out.print("'--------' ");
			break;
		} //switch		
	} //printSpades
	
	//displaying ansii back of the card
	private void printBack(int line) {
		switch (line){
		case 1:
			System.out.print(".--------. ");
			break;
		case 2:
			System.out.print("|        | ");
			break;
		case 3:
			System.out.print("|  o  o  | ");
			break;
		case 4:
			System.out.print("|   |    | ");
			break;
		case 5:
			System.out.print("|  \\__/  | ");
			break;
		case 6:
			System.out.print("'--------' ");
			break;
		} //switch		
	} //printBack
} //class
