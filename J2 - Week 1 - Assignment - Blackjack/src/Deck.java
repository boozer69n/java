import java.util.ArrayList;
import java.util.Random;

//assembling the deck of cards
public class Deck {
	private ArrayList< Card > cards = new ArrayList< Card >();
	
	//constructor
	public Deck(int numDeck, boolean shuffle){	
		for (int d = 0; d < numDeck; d++){ //decks
			for (int s = 0; s < 4; s++){ //suits
				for (int n = 1; n <= 13; n++){ //number
					Card c = new Card(Suit.values()[s], n); //creates card
					this.cards.add(c); //adds card to array
				}
			}
		}
		if (shuffle){
			this.shuffle();
		}
	} //Deck
	
	//using random to swap random cards
	public void shuffle(){
		Random rand = new Random();
		
		Card temp;
		int j;
		for (int i = 0; i < this.cards.size(); i++){ //first spot
			j = rand.nextInt(this.cards.size()); //random second spot
			//swapping
			temp = this.cards.set(i, this.cards.get(j));
			this.cards.set(j, temp);
		}
	} //shuffle

	//takes the top card and returns it
	public Card dealNextCard(){
		Card top = this.cards.remove(0);
		return top;
	} //dealNextCard
	
	//returns number of cards left in the deck
	public int deckSize(){
		return this.cards.size();
	}
} //Class