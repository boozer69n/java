//class containing the information of an object Card
public class Card {

	private Suit mySuit;
	private int myNumber;
	
	//constructor
	public Card(Suit suit, int number){
		this.mySuit = suit;
		if (number >= 1 && number <= 13){
			this.myNumber = number;
		}else{
			System.err.println(number + " is invalid");
			System.exit(1);
		}
	}
	
	//returns the number of the card
	public int getMyNumber(){
		return this.myNumber;
	}
	
	//returns the suit value
	public int getMySuit(){
		return this.mySuit.ordinal();
	}

	//overridden to get the correct number/letter for display purposes
	public String toString(){
		String numStr = "X";
		
		switch(this.myNumber){
		case 1:
			numStr = "A";
			break;
		case 2:
			numStr = "2";
			break;
		case 3:
			numStr = "3";
			break;
		case 4:
			numStr = "4";
			break;
		case 5:
			numStr = "5";
			break;
		case 6:
			numStr = "6";
			break;
		case 7:
			numStr = "7";
			break;
		case 8:
			numStr = "8";
			break;
		case 9:
			numStr = "9";
			break;
		case 10:
			numStr = "10";
			break;
		case 11:
			numStr = "J";
			break;
		case 12:
			numStr = "Q";
			break;
		case 13:
			numStr = "K";
			break;
		}
		return numStr;
	} //toString
} //class