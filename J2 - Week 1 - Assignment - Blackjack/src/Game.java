/**
Simplified Blackjack Game
In a blackjack game, each player plays against the dealer.  The objective of the Blackjack game
is to get closer to 21 than the dealer.  The dealer will first of all shuffle the deck.  Then
each player takes a turn.  The dealer will deal two cards face up to the first player.  The
player will then count his hand.  The player can �hit� (request a new card from the dealer)
until he reaches what he considers to be a satisfactory score (he �stands�) or he goes �bust�.
That is, the cards in his hand exceed 21.  The dealer will then deal two cards face up.  The
dealer in turn will �hit� until he exceeds the player�s total or goes �bust�.  The players turn
then ends and play continues with the next player.  When all players have played, play
continues with the next round.

Card values are as follows.  An ace is either worth 1 or 11, numbered cards are their face
value and face cards count as 10.

Assignment
Your assignment is to write a console blackjack game using the design you created in your
Assignment 1.1.

All of your user interface code should be in your Game class
Let the use-cases and class diagram drive your implementation. Implement the classes you
specified in your class diagram and insure the functionality from the use-cases is available
in your code.  
Extra credit of 10% if you make this a console game with card graphics.

The entire project folder and a runnable jar file should be submitted to the dropbox. 
 */
import java.util.Scanner;
import java.util.ArrayList;

public class Game {
	static ArrayList< Player > players = new ArrayList< Player >();
	static Deck theDeck;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args){
		startGame();
	}
	
	//setting up game and launching
	public static void startGame(){
		String input;
		int pCount;
		int dCount;
		//boolean shouldExit = false;
		
		System.out.print("\n[N]ew Game | [E]xit: ");
		input = sc.nextLine();
		
		if (input.compareToIgnoreCase("n") == 0){
			//getting number of players & decks
			pCount = intCheck("Number of Players: ");			
			
			//looping through number of players and adding to array
			Player p = new Player("Dealer");
			players.add(p);
			for (int i = 0; i < pCount; i++){
				System.out.printf("\nPlayer %d's Name:", i+1);
				p = new Player(sc.nextLine());
				players.add(p);
			}
			
			//getting number of decks and creating deck and shuffling
			dCount = intCheck("Number of Decks: ");
			theDeck = new Deck(dCount, true);
			
			dealCards(); //dealing cards
			playGame(); //begin game
			
		}else{
			System.exit(1);
		}
	} //startGame

	//checking for int input
	static int intCheck(String phrase){
		boolean shouldExit = false;
		int number = 0;
		
		while (!shouldExit){
			System.out.print(phrase);
			if(sc.hasNextInt()){
				number = sc.nextInt();
				sc.nextLine();
				shouldExit = true;
			}else{
				sc.nextLine();
			}			
		}
		return number;
	} //intCheck
	
	//deal to all the players 2 cards
	static void dealCards(){
		if (theDeck.deckSize() < players.size() * 5){
			System.out.println("Not enough cards to continue");
			System.out.println("Exiting");
			System.exit(1);
		}
		Player cPlayer;
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < players.size(); j++){
				cPlayer = players.get(j);
				//cPlayer.emptyHand();
				cPlayer.addCard(theDeck.dealNextCard());
			}
		}
	} //dealCards
	
	//starts the game and asks players to hit/stand
	static void playGame(){
		Player dealer = players.get(0);
		Player currentPlayer;
		boolean shouldExit = false;
		String hitStand;

		//looping through the players
		for(int i = 1; i < players.size(); i++){
			currentPlayer = players.get(i);
			shouldExit = false;

			while(!shouldExit){
				System.out.println("=========================================");
				dealer.printHand(false);
				currentPlayer.printHand(true);
				System.out.print("\n[H]it or [S]tand: ");
				hitStand = sc.nextLine();
				System.out.println("\n=========================================");
				if (hitStand.compareToIgnoreCase("h") == 0){
					if (!currentPlayer.addCard(theDeck.dealNextCard())){
						System.out.println("You BUSTED!!");
						shouldExit = true;
					}
				}else{
					shouldExit = true;
				} //if
			} //while
		} //for
		
		//running through dealer hand
		shouldExit = false;
		while(!shouldExit){
			if(dealer.getSum(true) < 17){				
				if (!dealer.addCard(theDeck.dealNextCard())){				
					System.out.printf("%s BUSTED!!", dealer.getName());
				}
			}else{
				shouldExit = true;
			} //if
		} //while

		dealer.printHand(true);
		gameResults();
		whatNext();
	} //playGame
	
	//displays the results of the hand
	static void gameResults(){
		System.out.println("\nResults of the hand\n----------------------------------------------");
		for (int i = 0; i < players.size(); i++){
			if (players.get(i).getSum(true) <= 21){
				System.out.printf("\n%s's Score: %d - %s", players.get(i).getName(), players.get(i).getSum(true), winLose(i));
			}else{
				System.out.printf("\n%s's Score: %s - %s", players.get(i).getName(), "BUSTED!", "Lost");
			} //if			
		} //for
	} //gameResults
	
	//returns whether you win/lost/push 
	static String winLose(int player){
		int count = 0;
		if (player == 0){
			for(int i = 1; i < players.size(); i++){
				if (players.get(0).getSum(true) < players.get(i).getSum(true) && players.get(i).getSum(true) <= 21){
					return "Lost";
				}
				if (players.get(0).getSum(true) > players.get(i).getSum(true) && players.get(0).getSum(true) <= 21){
					count++;
				}
			}
			if(count == players.size()-1){return "Winner";}
			else{return "Lost";}
		}
		if (players.get(0).getSum(true) > 21){return "Winner";}
		if (players.get(0).getSum(true) < players.get(player).getSum(true)){return "Winner";}
		if (players.get(0).getSum(true) == players.get(player).getSum(true)){return "Push";}
		return "Lost";
	} //winLose
	
	//asking whether to continue or exit
	static void whatNext(){
		Player cPlayer;
		String input;
		System.out.println("");
		System.out.println("[C]ontinue");
		System.out.print("[E]xit: ");
		input = sc.nextLine();
		switch(input.toLowerCase()){
		case "c":
			for (int i = 0; i < players.size(); i++){
				cPlayer = players.get(i);
				cPlayer.emptyHand();
			}
			dealCards();
			playGame();
			break;
		default:
			System.exit(1);
			break;
		}
	} //whatNext
} //class