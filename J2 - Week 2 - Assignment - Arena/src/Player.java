/**
Student: Clinton Booze
Professor: Jill Coddington
Class: Java II (CSC263)
Assignment: Arena
===================================================================================================
*/

public class Player {
	private int strength;
	private int defense;
	private int intelligence;
	private int life;
	private int title;
	private String name;
	private String[] titleArray = {"Wuss", "Weak", "Strong", "Femanin", "Skipper",
			"Chubby", "Plump", "Merry", "Handsome", "Georgeous"};
	
	/**
	 * Base class for player
	 * @param str = strength of enemy
	 * @param def = defense of the enemy
	 * @param intel = intelligence of the enemy
	 * @param pName = name of the player "Clinton the Novice"
	 * @param life = health of the player
	 */
	public Player(int str, int def, int intel, String pName, int title){
		this.setStrength(str);
		this.setDefense(def);
		this.setIntelligence(intel);
		this.name = pName;
		this.life = 150;
		this.title = title;
		updateName();
	} //constructor
	
	/**
	 * Assigning a random title to the play just for fun
	 */
	private void updateName() {
		String newName = this.name;
		newName += " the ";
		newName += titleArray[this.title];
		this.name = newName;		
	}

	/**
	 * Displays the stat information about the player
	 * Name, Strength, Defense, Intelligence
	 */
	public void infoDisplay(){
		System.out.printf("Name: %s\n", this.name);
		System.out.printf("Strength: %d\n", this.strength);
		System.out.printf("Defense: %d\n", this.defense);
		System.out.printf("Intelligence: %d\n", this.intelligence);
		System.out.printf("Health: %d\n", this.life);
	} //baseDisplay

	/**
	 * @return
	 * 		Returns player strength
	 */
	public int getStrength() {
		return this.strength;
	} //getStrength

	/**
	 * @param strength
	 * 		Sets the player strength
	 */
	public void setStrength(int strength) {
		this.strength = strength;
	} //setStrength

	/**
	 * @return
	 * 		Returns the player defense
	 */
	public int getDefense() {
		return this.defense;
	} //getDefense

	/**
	 * @param defense
	 * 		Sets the player defense
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	} //setDefense

	/**
	 * @return
	 * 		Returns the player Intelligence
	 */
	public int getIntelligence() {
		return this.intelligence;
	} //getIntelligence

	/**
	 * @param intelligence
	 * 		Sets the player Intelligence
	 */
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	} //setIntelligence
	
	/**
	 * @return
	 * 		Returns the player name
	 */
	public String getName() {
		return this.name;
	} //getName

	/**
	 * @param eName
	 * 		Sets the player Name
	 */
	public void setName(String pName) {
		this.name = pName;
	} //setName
	
	/**
	 * @return
	 * 		Returns life of the player
	 */
	public int getLife() {
		return this.life;
	} //getLife
	
	/**
	 * @param life
	 * 		Sets the life of the player
	 */
	public void setLife(int life) {
		this.life = life;
	} //setLife

} //Player