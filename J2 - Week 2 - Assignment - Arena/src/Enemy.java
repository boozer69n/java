/**
Student: Clinton Booze
Professor: Jill Coddington
Class: Java II (CSC263)
Assignment: Arena
===================================================================================================
*/

public abstract class Enemy {
	private int strength;
	private int defense;
	private int intelligence;
	private int life;
	private String name;
	
	/**
	 * Base class for all enemies
	 * @param str = strength of enemy
	 * @param def = defense of the enemy
	 * @param intel = intelligence of the enemy
	 * @param eName = name of the enemy "Narrg the Humanoid"
	 * @param life = health of the enemy
	 */
	public Enemy(int str, int def, int intel, int life){
		this.strength = str;
		this.defense = def;
		this.intelligence = intel;
		this.life = life;
	} //constructor
	
	/**
	 * Displays the stat information about the enemy
	 * Name, Strength, Defense, Intelligence
	 */
	public void infoDisplay(){
		System.out.printf("Name: %s\n", this.name);
		System.out.printf("Strength: %d\n", this.strength);
		System.out.printf("Defense: %d\n", this.defense);
		System.out.printf("Intelligence: %d\n", this.intelligence);
	} //baseDisplay
	
	public abstract int defenseBonus(int incDmg);
	public abstract int attackBonus(int incDmg);

	/**
	 * @return
	 * 		Returns enemy strength
	 */
	public int getStrength() {
		return this.strength;
	} //getStrength

	/**
	 * @param strength
	 * 		Sets the enemy strength
	 */
	public void setStrength(int strength) {
		this.strength = strength;
	} //setStrength

	/**
	 * @return
	 * 		Returns the enemy defense
	 */
	public int getDefense() {
		return this.defense;
	} //getDefense

	/**
	 * @param defense
	 * 		Sets the enemy defense
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	} //setDefense

	/**
	 * @return
	 * 		Returns the enemy Intelligence
	 */
	public int getIntelligence() {
		return this.intelligence;
	} //getIntelligence

	/**
	 * @param intelligence
	 * 		Sets the enemy Intelligence
	 */
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	} //setIntelligence
	
	/**
	 * @return
	 * 		Returns the enemy name
	 */
	public String getName() {
		return this.name;
	} //getName

	/**
	 * @param eName
	 * 		Sets the enemy Name
	 */
	public void setName(String eName) {
		this.name = eName;
	} //setName
	
	/**
	 * @return
	 * 		Returns life of the enemy
	 */
	public int getLife() {
		return this.life;
	} //getLife
	
	/**
	 * @param life
	 * 		Sets the life of the enemy
	 */
	public void setLife(int life) {
		this.life = life;
	} //setLife
} //Enemy