/**
Student: Clinton Booze
Professor: Jill Coddington
Class: Java II (CSC263)
Assignment: Arena
===================================================================================================
*/

public class Orc extends Enemy{
	
	private int orcBonus;
	private String[] nameArray = {"Glug", "Balrog", "Garrg", "Brock", "Billy",
			"Rawr", "Ugg", "Nugg", "Grrr", "Burp"};
	private String[] titleArray = {"Flabby", "Huge", "Skinny", "Strong", "Snorer",
			"Wheebley", "Fat", "Chubby", "Grunter", "Backwards"};
	private int bName;
	private int eTitle;

	/**
	 * Creating an orc enemy
	 * @param str = strength of the enemy
	 * @param def = defense of the enemy
	 * @param intel = intelligence of the enemy
	 * @param beginName = random number 0-9 for random name
	 * @param endTitle = random number 0-9 for random title
	 * @param life = health of the enemy
	 */
	public Orc(int str, int def, int intel, int beginName, int endTitle, int life) {
		super(str, def, intel, life);
		this.orcBonus = str;
		this.bName = beginName;
		this.eTitle = endTitle;
		setOrcName();
	} //constructor
	
	/**
	 * @param incDmg = original damage before modification
	 * @return
	 * 		Returns the +% damage of the enemy
	 */
	public int attackBonus(int incDmg){
		int newDmg = incDmg + this.orcBonus;
		return newDmg;
	} //attackBonus
	
	/**
	 * @param incDmg = original damage before modification
	 * @return
	 * 		Returns the bonus defense
	 */
	public int defenseBonus(int incDmg){
		return incDmg;
	} //defenseBonus
	
	/**
	 * @return
	 * 		Returns the race of the enemy
	 */
	public String getRace(){
		return "Orc";
	} //getRace
	
	/**
	 * Displays info on the enemy using polymorphism
	 */
	@Override
	public void infoDisplay(){
		System.out.printf("Race: %s\n", this.getRace());
		super.infoDisplay();
	} //infoDisplay
	
	/**
	 * Setting the name of the enemy using random numbers and an array
	 */
	private void setOrcName(){
		String orcName = "";
		orcName += nameArray[this.bName];
		orcName += " the ";
		orcName += titleArray[this.eTitle];
		super.setName(orcName);
	} //setName
} //Orc