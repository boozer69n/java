/**
Student: Clinton Booze
Professor: Jill Coddington
Class: Java II (CSC263)
Assignment: Arena
===================================================================================================
*/

public class Human extends Enemy{
	
	private int humanBonus;
	private String[] nameArray = {"Bob", "Sally", "Mark", "Mat", "Thom",
			"Smiley", "Lester", "Richard", "Chuck", "Wally"};
	private String[] titleArray = {"Limp", "Depressed", "Chipper", "Weak", "Sleepy",
			"Wobbeley", "Fat", "Chubby", "Drooley", "DimWhit"};
	private int bName;
	private int eTitle;

	/**
	 * Creating a human enemy
	 * @param str = strength of the enemy
	 * @param def = defense of the enemy
	 * @param intel = intelligence of the enemy
	 * @param beginName = random number 0-9 for random name
	 * @param endTitle = random number 0-9 for random title
	 * @param life = health of the enemy
	 */
	public Human(int str, int def, int intel, int beginName, int endTitle, int life) {
		super(str, def, intel, life);
		this.humanBonus = def;
		this.bName = beginName;
		this.eTitle = endTitle;
		setHumanName();
	} //constructor
	
	/**
	 * @param incDmg = original damage before modification
	 * @return
	 * 		Returns the +% damage of the enemy
	 */
	public int attackBonus(int incDmg){
		return incDmg;
	} //attackBonus
	
	/**
	 * @param incDmg = original damage before modification
	 * @return
	 * 		Returns the bonus defense
	 */
	public int defenseBonus(int incDmg){
		int newDmg = incDmg - (this.humanBonus /2);
		return newDmg;
	} //defenseBonus
	
	/**
	 * @return
	 * 		Returns the race of the enemy
	 */
	public String getRace(){
		return "Human";
	} //getRace
	
	/**
	 * Displays info on the enemy using polymorphism
	 */
	@Override
	public void infoDisplay(){
		System.out.printf("Race: %s\n", this.getRace());
		super.infoDisplay();
	} //infoDisplay
	
	/**
	 * Setting the name of the enemy using random numbers and an array
	 */
	private void setHumanName(){
		String humanName = "";
		humanName += nameArray[this.bName];
		humanName += " the ";
		humanName += titleArray[this.eTitle];
		super.setName(humanName);
	} //setName
} //Human