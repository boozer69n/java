/**
Student: Clinton Booze
Professor: Jill Coddington
Class: Java II (CSC263)
Assignment: Arena
===================================================================================================
Assignment
Create a game where there are 3 classes for enemies. The base class is enemy which should have a
minimum of 3 attributes and a minimum of 8 methods (1 get and set method for each attribute, a
constructor, and a display method).

Create a player class with a minimum of 3 attributes and a minimum of 8 methods (1 get and set
method for each attribute, a constructor, and a display method).

The game must be playable � the player should be confronted by an enemy then given the choice of
attacking, defending, or fleeing. The outcomes for the 2 inherited enemies must be different but
it should not be 100% skill based � there should be a random factor included.

The game must keep life scores and determine a winner (ex � killed 3 enemies = win, life = 0 for
player = lose).

The code must be thoroughly tested using JUnit tests, and the entire project folder and a
runnable jar file should be submitted to the dropbox.
===================================================================================================
*/
import java.util.Random;
import java.util.Scanner;

public class Arena {
	static Scanner sc = new Scanner(System.in);
	static Random rand = new Random();
	static Player myPlayer;

	public static void main(String[] args) {
		ui();
	}
	
	/**
	 * Prompts the user for a selection on what to do
	 */
	static void ui(){
		boolean shouldExit = false;
		String input;
		while(!shouldExit){			
			System.out.println("[S]tart Game");
			System.out.println("[I]nstructions");
			System.out.println("[E]xit");
			System.out.print("Selection: ");
			input = sc.nextLine().toLowerCase();
			switch(input){
			case "s":
				createPlayer();
				for(int i = 0; i < 5; i++){
					startFight(returnMonster());
				}
				System.out.println("You Win!");
				System.exit(1);
				break;
			case "i":
				instructions();
				break;
			default:
				System.exit(1);
				break;
			}
		}
	} //ui
	
	/**
	 * Displays all information on the game
	 */
	static void instructions(){
		System.out.println("\n\n");
		System.out.println("================================");
		System.out.println("Objective: Pass 5 enemies");
		System.out.println("You will be given 3 options");
		System.out.println("Attack, Heal, Run: If you run you will be attacked twice");
		System.out.println("================================");
		System.out.println("Stats:");
		System.out.println("Strength: 5 damage per strength");
		System.out.println("Defense: 1 damage reduction per defense");
		System.out.println("Intelligence: 1 heal per intelligence (overheal possible)");
		System.out.println("================================");
		System.out.println("Enemies:");
		System.out.println("There are 2 types of enemies; orcs and humans");
		System.out.println("Orcs: Do additional damage based off of strength");
		System.out.println("Humans: take decreased damage based off of defense");
		System.out.println("Options: The enemies have 3 options");
		System.out.println("1% chance to run, 30% chance to heal, 60% chance to attack");
		System.out.println("================================");
		System.out.println("Warning:");
		System.out.println("0 Strength = No damage");
		System.out.println("0 Defense = You take full damage");
		System.out.println("0 Intelligence = No healing");
		System.out.println("You will have the ability to attack and heal but nothing will happen");
		System.out.println("================================\n\n");
	}
	
	/**
	 * Performs a fight between the player and an enemy
	 * @param badGuy
	 */
	static void startFight(Enemy badGuy){
		String selection;
		int monsterAction;
		System.out.println("================================");
		System.out.println("\nEnemy Info");
		System.out.println("================================");
		badGuy.infoDisplay();
		System.out.println("================================");
		System.out.println("Your Info");
		System.out.println("================================");
		myPlayer.infoDisplay();
		System.out.println("================================\n");
		while(!deathCheck(badGuy) && !deathCheck(myPlayer)){
			System.out.printf("\nYour Health: %d\n", myPlayer.getLife());
			System.out.printf("Enemy Health: %d\n", badGuy.getLife());
			
			options();
			selection = sc.nextLine().toLowerCase();
			switch(selection){
			case "a":
				attack(myPlayer, badGuy);
				break;
			case "h":
				heal(myPlayer);
				break;
			default:
				for(int i = 0; i < 2; i++){
					attack(badGuy, myPlayer);
					if(deathCheck(myPlayer)){
						System.out.println("You Died");
						System.exit(1);
					}
				}
				break;
			} //switch
			if (badGuy.getLife() > 0){
				monsterAction = rand.nextInt(10) + 1;
				if(monsterAction == 1){
					badGuy.setLife(0);
					System.out.printf("%s Ran away!\n", badGuy.getName());
				}else if(monsterAction > 1 && monsterAction <= 4){
					heal(badGuy);
					System.out.printf("%s Healed\n", badGuy.getName());
				}else{
					attack(badGuy, myPlayer);
					System.out.printf("%s Attacked\n", badGuy.getName());
				}
			}
		} //while
	} //startFight

	/**
	 * Displaying the options for the player
	 */
	static void options(){
		System.out.println("[A]ttack");
		System.out.println("[H]eal");
		System.out.println("[R]un");
		System.out.print("Selection: ");
	}
	
	/**
	 * Performs an attack on the player from the enemy
	 * @param badGuy
	 * @param goodGuy
	 */
	static void attack(Enemy badGuy, Player goodGuy){
		int health = goodGuy.getLife();
		int dmg = 5 * badGuy.getStrength();
		int dmgBonus = badGuy.attackBonus(dmg);
		int afterDef = dmgBonus - goodGuy.getDefense();
		int newHealth = health - afterDef;
		if(newHealth < health){
			goodGuy.setLife(newHealth);
		}
	}
	
	/**
	 * Performs an attack on the monster from the player
	 * @param goodGuy
	 * @param badGuy
	 */
	static void attack(Player goodGuy, Enemy badGuy){
		int health = badGuy.getLife();
		int dmg = 5 * goodGuy.getStrength();
		int afterDef = dmg - badGuy.getDefense();
		int bonusDef = badGuy.defenseBonus(afterDef);
		int newHealth = health - bonusDef;
		if(newHealth < health){
			badGuy.setLife(newHealth);
		}
	}
	
	/**
	 * Performs a heal on the player
	 * @param goodGuy
	 */
	static void heal(Player goodGuy){
		goodGuy.setLife(goodGuy.getLife() + goodGuy.getIntelligence());
	}
	
	/**
	 * Performs a heal on the enemy
	 * @param badGuy
	 */
	static void heal(Enemy badGuy){
		badGuy.setLife(badGuy.getLife() + badGuy.getIntelligence());
	}
		
	/**
	 * @param obj
	 * @return
	 * 		Returning whether or not the enemy is dead
	 * 		Overloaded to accept Player also
	 */
	static boolean deathCheck(Enemy obj){
		if (obj.getLife() <= 0){
			return true;
		}
		return false;
	} //lifeCheck
	
	/**
	 * @param obj
	 * @return
	 * 		Returning whether or not the player is dead
	 * 		Overloaded to accept Enemy also
	 */
	static boolean deathCheck(Player obj){
		if (obj.getLife() <= 0){
			return true;
		}
		return false;
	} //lifeCheck
	
	/**
	 * Creates player per user point distribution
	 */
    static void createPlayer(){
        String input;
        String myName;
        int str = 0;
        int def = 0;
        int intel = 0;
        
        int title = rand.nextInt(10);
        System.out.print("Please enter Name: ");
        myName = sc.nextLine();
        for(int i = 0; i < 20; i++){
            System.out.printf("\nYou have %d points left\n", 20-i);
            System.out.printf("[S]trength: %d\n", str);
            System.out.printf("[D]efense %d\n", def);
            System.out.printf("[I]ntelligence %d\n", intel);
            System.out.print("Selection: ");
            input = sc.nextLine().toLowerCase();
            switch(input){
            case "s":
                str++;
                break;
            case "d":
                def++;
                break;
            default:
                intel++;
                break;
            }
        }
        myPlayer = new Player(str, def, intel, myName, title);
    } //createPlayer
	
	/**
	 * @return
	 * 		Returns an enemy for the player to fight with random stats and name
	 */
	static Enemy returnMonster(){
		int beginName = rand.nextInt(10);
		int endTitle = rand.nextInt(10);
		int beginLife = rand.nextInt(100) + 50;
		int str = 0;
		int def = 0;
		int intel = 0;
		int temp = 0;
		for(int i = 0; i < 10; i++){
			temp = rand.nextInt(3)+1;
			switch(temp){
			case 1:
				str++;
				break;
			case 2:
				def++;
				break;
			case 3:
				intel++;
				break;
			}
		}
		switch(rand.nextInt(2)){
		case 0:
			return new Human(str, def, intel, beginName, endTitle, beginLife);
		default:
			return new Orc(str, def, intel, beginName, endTitle, beginLife);
		}
	} //returnMonster
	
} //Arena