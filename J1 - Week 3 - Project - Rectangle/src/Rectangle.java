//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 3 - Project - Decision Structures
//#################################################################################################
//The purpose of this project is to familiarize you with the basic flow control concepts in Java
//
//if, and if...else decision logic structures 
//while looping structures 
//You must write a Java program that allows the user to input two integer values. The first
//integer will be a height value, and the second integer will be a width value. Your program will
//then display a rectangle (whose sides are composed of asterisks) that is the given height and
//width. The User interface of your program should be clear and easy to read and understand.
//
//Example: If the user enters 5 for the height and 7 for the width, then you program will output
//to the console a rectangle like this:
//
// *******
// *     *
// *     *
// *     *
// *******

//For this project you must: 

//Use at least one if decision structure. 
//Use at least one while loop. 
//#################################################################################################

import java.util.Scanner;

public class Rectangle { //begin class
    public static void main(String[] args) { //begin main
    
	RecSize rect = new RecSize(); //creating RecSize object
	Scanner i = new Scanner(System.in); //opening scanner object
	
	String h = "Height"; //height string
	String w = "Width"; //width string
	
	enterLength(rect, i, h); //method to set height
	enterLength(rect, i, w); //method to set width
	display(rect); //method to draw the rectangle
	
	i.close(); //closing scanner object
	}
	
	static void enterLength(RecSize rec, Scanner i, String side){ //method to set height and width
		boolean shouldExit = false; //exit variable
		while (!shouldExit){ //begin loop
			System.out.println(""); //for display
			System.out.print(side + ": "); //requesting height
			if (i.hasNextInt()){ //verifying input is an integer
			    int posCheck = i.nextInt(); //saving in to temp variable
                i.nextLine(); //removing return
                if (posCheck > 0){ //checking to make sure its positive
					if (side.equals("Height")){ //if for height
						rec.setHeight(posCheck); //setting height
						shouldExit = true; //setting exit variable
					}else if(side.equals("Width")){ //if for width
						rec.setWidth(posCheck); //setting width
						shouldExit = true; //setting exit variable
					}else{ //begin else
						System.out.println("Must be a Height or a Width"); //prompt if invalid
					} //end else
                }else{ //begin else
                    System.out.println("Must be Posative"); //prompt if invalid
                } //end else
			}else{ //begin else
				System.out.println("Must be Integer"); //prompt if invalid
				i.nextLine(); //to remove return
			} //end else
		} //end loop
	} //end method
	
	static void display(RecSize rec){ //method to display rectangle
		
		int rowEnd = rec.getHeight(); //saving defined height to local variable
		int colEnd = rec.getWidth(); //saving defined width to local variable
		int row = 1; //setting start location for row
		int col = 1; //setting start location for col
		
		System.out.println(""); //for display
		System.out.println("Height: " + rowEnd); //displaying user defined height
		System.out.println(" Width: " + colEnd); //displaying user defined width
		System.out.println("  Area: " + (rowEnd * colEnd)); //for display
		System.out.println(""); //for display

		
		boolean shouldExit = false; //setting exit variable
		while(!shouldExit){ //begin loop
			if (rowEnd == 1){ //if the rectangle is only 1 tall
				while(col <= colEnd){ //while loop cycling through the width
					System.out.print("*"); //print * for top row
					col++; //to cycle through width
					} //end loop
					shouldExit = true; //exit main loop
			}else if(colEnd == 1){ //if the rectangle is only 1 wide
				while(row <= rowEnd){ //while loop cycling through the height
					System.out.println("*"); //print * for column
					row++; //to cycle through height
					} //end loop
					shouldExit = true; //exit main loop
			}else{ //begin else
				if(row == 1){ //if statement for the first row of the rectangle
					while(col <= colEnd){ //while loop cycling through the width
					System.out.print("*"); //print * for top row
					col++; //to cycle through width
					} //end loop
					System.out.println(""); //next line
					col = 1; //reset column
					row++; //next row
				} //end if
				if(row == rowEnd){ //print * for bottom row
					while(col <= colEnd){ //while loop cycling through width
					System.out.print("*"); //print * for bottom row
					col++; //to cycle through width
					} //end loop
					shouldExit = true; //exit main loop
				} //end if
				if(col == 1){ //if for left side of rectangle
					System.out.print("*"); //to print * for left side
					col++; //increase col
				}else if(col == colEnd){ //if else for right side
					System.out.print("*"); //to print * for right side
					System.out.println(""); //go to next row
					col = 1; //reset col
					row++; //next row
				}else{ //else for empty part of rectangle
					System.out.print(" "); //prints empty space
					col++; //increases col
				}
			}
		}
	}
}