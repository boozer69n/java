//Name: Clinton Booze
//Professor: Jason Turner
//Class: Java Programming I (CSC203)
//Week 3 - Project - Decision Structures
//#################################################################################################
//The purpose of this project is to familiarize you with the basic flow control concepts in Java
//
//if, and if...else decision logic structures 
//while looping structures 
//You must write a Java program that allows the user to input two integer values. The first
//integer will be a height value, and the second integer will be a width value. Your program will
//then display a rectangle (whose sides are composed of asterisks) that is the given height and
//width. The User interface of your program should be clear and easy to read and understand.
//
//Example: If the user enters 5 for the height and 7 for the width, then you program will output
//to the console a rectangle like this:
//
// *******
// *     *
// *     *
// *     *
// *******

//For this project you must: 

//Use at least one if decision structure. 
//Use at least one while loop. 
//#################################################################################################

public class RecSize { //begin class
	private int height; //stores age
	private int width; //stores rating
	
	public int getHeight() { //begin method
		return this.height; //returns height
	} //end method

	public void setHeight(int height) { //begins method
		this.height = height; //sets height
	} //end method
	
		public int getWidth() { //begin method
		return this.width; //returns width
	} //end method

	public void setWidth(int width) { //begins method
		this.width = width; //sets width
	} //end method
}