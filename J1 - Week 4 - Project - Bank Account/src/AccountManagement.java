/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 4 - Project - Bank Account
###################################################################################################
The purpose of this project is to familiarize you with the basic concepts of writing classes and
instantiating and manipulating objects of user-defined classes in Java.

You must write a Java program that allows the user to create and maintain a collection of
BankAccount objects.

You must write a Java class called BankAccount which contains the following properties:

AccountNumber � a String that is read only 
AccountHolderName � a String 
CurrentBalance � an float that is read only 
Your BankAccount class must also have the following methods:

Deposit � takes a positive float amount as an argument and increases CurrentBalance by this amount 
Withdrawal - takes a positive float amount as an argument and decreases CurrentBalance by this amount 
Your program will present the user with a menu � the choices from this menu will be:

Create New Account 
Delete Account 
View Account Info 
Make Deposit 
Make Withdrawal 
Exit Program 

If the user selects Create New Account, they will be asked to provide the account holder's name.
A new BankAccount object will be created � account numbers will be assigned sequentially
(starting at 1). New BankAccounts will have a CurrentBalance of 0. After this, the menu will be
displayed again.

If the user selects Delete Account, they will be asked to provide the account number. Any
BankAccount object in the collection with the matching account number will be removed from
the collection. After this, the menu will be displayed again.

If the user selects View Account Info, they will be asked to provide the account number. All
info (AccountNumber, AccountHolderName, CurrentBalance) for the account with that account
number will be displayed. After this, the menu will be displayed again.

If the user selects Make Deposit, they will be asked to provide the account number, and the
deposit amount. Any BankAccount object in the collection with the matching account number will
have its current balance incremented by the deposit amount. After this, the menu will be
displayed again.

If the user selects Make Withdrawal, they will be asked to provide the account number,
and the withdrawal amount. Any BankAccount object in the collection with the matching
account number will have its current balance decremented by the withdrawal amount. After
this, the menu will be displayed again.

If the user selects Exit Program, they program will end.

The User interface of your program should be clear and easy to read and understand.
#################################################################################################*/
import java.text.DecimalFormat; //to display the info correctly
import java.util.Scanner; //for input
import java.util.ArrayList; //for arraylist

public class AccountManagement { //begin class
	static DecimalFormat acctFormat = new DecimalFormat("0000"); //setting format for account display
	static Scanner i = new Scanner(System.in); //scanner object
	static ArrayList< BankAccount > accounts = new ArrayList< BankAccount >(); //creating array list
	
	public static void main( String[] args ){ //begin main
		boolean shouldExit = false; //exit variable
		while(!shouldExit){ //begin loop
			//ui of options
			System.out.println("");
			System.out.println("1 - New Account");
			if(checkAccount()){ //if to show additional options if there are accounts
				System.out.println("2 - Delete Account");
				System.out.println("3 - View Account Info");
				System.out.println("4 - Make Deposit");
				System.out.println("5 - Make Withdrawal");
				System.out.println("6 - Change Name");
			} //end if
			System.out.println("7 - Exit");
			System.out.print("Selection: ");
			
			if(i.hasNextInt()){ //making sure an int is entered
				int userSelection = i.nextInt(); //saving int
				i.nextLine(); //removing return
				if(checkAccount() || userSelection == 1 || userSelection == 7){ //checking for valid input
					switch(userSelection){ //begin switch
						case 1: //begin 1
							newAccount(); //new account method
							break; //end 1
						case 2: //begin 2
							delAccount(getAccount()); //delete account method
							break; //end 2
						case 3: //begin 3
							getAccount().display(); //displaying account info
							break; //end 3
						case 4: //begin 4
							deposit(getAccount(), ammount()); //deposit method
							break; //end 4
						case 5: //begin 5
							withdrawal(getAccount(), ammount()); //withdrawal method
							break; //end 5
						case 6: //begin 6
							newName(getAccount()); //new name method
							break; //end 6
						case 7: //begin 7
							System.out.println("Bye o/"); //waving bye
							shouldExit = true; //exit variable
							break; //end 7
						default: //begin default
							System.out.println("Invalid Input"); //if invalid
							break; //end default
					} //end switch
				}else{ //begin else
					System.out.print("Invalid Input"); //invalid prompt
					System.out.println(""); //for display
				} //end else
			}else{ //begin else if an int wasn't entered
				System.out.print("Invalid Input"); //invalid prompt
				System.out.println(""); //for display
				i.nextLine(); //remove return
			} //end else
		} //end loop
	} //end main
	
	public static BankAccount newAccount(){ //creating a new account
		System.out.println(""); //for display
		System.out.print("Please enter name: "); //requesting name
		String name = i.nextLine(); //saving name
		BankAccount acct = new BankAccount(name); //creating bank account object
		accounts.add(acct); //adding object to array list
		System.out.println("Account Number Assigned: " + acctFormat.format(acct.getAccNum())); //displaying account number
		return acct; //returning account object for possible future functionality
	} //end method
	
	public static boolean delAccount(BankAccount acct){ //deleting the account
		return accounts.remove(acct); //returns boolean for possible additional functionality
	} //end method
	
	public static void deposit(BankAccount acct, float ammt){ //making a deposit
		acct.setDeposit(ammt); //setting deposit
	} //end method
	
	public static void withdrawal(BankAccount acct, float ammt){ //making a withdrawal
		float balance = acct.getBalance(); //saving objects balance
		if((balance - ammt) > 0){ //checking if enough money is in account
			acct.setWithdrawal(ammt); //if enough take out
		}else{ //else
			System.out.println("You do not have enough money"); //display your broke
		} //end else
	} //end method
	
	public static void newName(BankAccount acct){ //changing name on account
		System.out.print("New Name: "); //requesting new name
		String name = i.nextLine(); //saving name
		acct.changeName(name); //changing name in object
	} //end method
	
	public static BankAccount getAccount(){ //returning account based upon account number
		while(true){ //begin loop
			System.out.println(""); //for display
			System.out.print("Account Number: "); //requesting account number
			if (i.hasNextInt()){ //confirming an int was entered
				int accNum = i.nextInt(); //saving int
				i.nextLine(); //removing return
				if (accNum > 0){ //saving processing time incase negative num was entered
					for (int j = 0; j < accounts.size(); j++){ //begin loop
						if(accNum == accounts.get(j).getAccNum()){ //looking for account number
							return accounts.get(j); //returns correct account object that matches number
						} //end if
					} //end loop
				} //end if
			}else{ //begin else
				i.nextLine(); //remove return
			} //end else
			System.out.println("Invalid Account Number"); //if invalid number is entered
		} //end loop
	} //end method
	
	public static float ammount(){ //requesting a dollar amount from user
		while (true){ //begin loop
			System.out.println(""); //for display
			System.out.print("Ammount: "); //requesting ammount
			if (i.hasNextFloat()){ //checking for float
				float money = i.nextFloat(); //saving float
				i.nextLine(); //removing return
				money = (money < 0 ? -money : money); //making sure number is positive
				return money; //returning number
			}else if (i.hasNextInt()){ //checking for int
				float money = i.nextInt(); //saving int
				i.nextLine(); //removing return
				money = (money < 0 ? -money : money); //making sure number is positive
				return money; //returning number
			}else{ //begin else
				System.out.println("Invalid Input"); //invalid input prompt
			} //end else
		} //end loop
	} //end method
	
	public static boolean checkAccount(){ //returning whether or not the array has any accounts
		if(accounts.size() > 0){ //checking that there is at least 1 account
			return true; //if so return true
		}else{ //begin else
			return false; //else return false
		} //end else
	} //end method
	
} //end class