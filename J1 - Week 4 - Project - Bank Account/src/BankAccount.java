/**
Name: Clinton Booze
Professor: Jason Turner
Class: Java Programming I (CSC203)
Week 4 - Project - Bank Account
###################################################################################################
The purpose of this project is to familiarize you with the basic concepts of writing classes and
instantiating and manipulating objects of user-defined classes in Java.

You must write a Java program that allows the user to create and maintain a collection of
BankAccount objects.

You must write a Java class called BankAccount which contains the following properties:

AccountNumber � a String that is read only 
AccountHolderName � a String 
CurrentBalance � an float that is read only 
Your BankAccount class must also have the following methods:

Deposit � takes a positive float amount as an argument and increases CurrentBalance by this amount 
Withdrawal - takes a positive float amount as an argument and decreases CurrentBalance by this amount 
Your program will present the user with a menu � the choices from this menu will be:

Create New Account 
Delete Account 
View Account Info 
Make Deposit 
Make Withdrawal 
Exit Program 

If the user selects Create New Account, they will be asked to provide the account holder's name.
A new BankAccount object will be created � account numbers will be assigned sequentially
(starting at 1). New BankAccounts will have a CurrentBalance of 0. After this, the menu will be
displayed again.

If the user selects Delete Account, they will be asked to provide the account number. Any
BankAccount object in the collection with the matching account number will be removed from
the collection. After this, the menu will be displayed again.

If the user selects View Account Info, they will be asked to provide the account number. All
info (AccountNumber, AccountHolderName, CurrentBalance) for the account with that account
number will be displayed. After this, the menu will be displayed again.

If the user selects Make Deposit, they will be asked to provide the account number, and the
deposit amount. Any BankAccount object in the collection with the matching account number will
have its current balance incremented by the deposit amount. After this, the menu will be
displayed again.

If the user selects Make Withdrawal, they will be asked to provide the account number,
and the withdrawal amount. Any BankAccount object in the collection with the matching
account number will have its current balance decremented by the withdrawal amount. After
this, the menu will be displayed again.

If the user selects Exit Program, they program will end.

The User interface of your program should be clear and easy to read and understand.
#################################################################################################*/

import java.text.DecimalFormat;

public class BankAccount { //begin class
	DecimalFormat acctFormat = new DecimalFormat("0000");
	DecimalFormat moneyFormat = new DecimalFormat("0.00");
	
	private static int nextAccountNumber = 1; //for account number, only initiates once
	
	private int accountNumber; //contains account number
	private String accountHolderName; //contains account holders name
	private float currentBalance; //contains account balance
	
	public BankAccount(String accName){ //constructor
		this.accountNumber = nextAccountNumber++; //sets account number
		this.accountHolderName = accName; //sets account holders name
	}
	
	public int getAccNum(){ //begin method
		return this.accountNumber; //returns account number
	} //end method
	
	public void setDeposit(float money){ //used for transactions
		this.currentBalance = (this.currentBalance + money); //updates the account
	} //end method
	
	public void setWithdrawal(float money){ //used for transactions
		this.currentBalance = (this.currentBalance - money); //updates the account
	} //end method
	
	public float getBalance(){ //returns account balance
		return this.currentBalance; //returns balance
	} //end method
	
	public String getName(){ //returns name
		return this.accountHolderName; //returns name
	} //end method
	
	public void changeName(String newName){ //method for updating/modifying name
		this.accountHolderName = newName; //sets new name
	} //end method
	
	public void display(){ //displays info on account
		System.out.println("Account: " + acctFormat.format(this.accountNumber)); //displays account number
		System.out.println("Name: " + this.accountHolderName); //displays account holders name
		System.out.println("Balance: $" + moneyFormat.format(this.currentBalance)); //displays current balance
	} //end method
} //end class